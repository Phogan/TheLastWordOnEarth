//
//  ViewController.swift
//  TheLastWordOnEarthTV
//
//  Created by Patrick Hogan on 26/10/2015.
//  Copyright © 2015 Patrick Hogan. All rights reserved.
//

import UIKit
import CoreData

class RootViewController: UIViewController {

    @IBOutlet weak var mainContainer: UIView!
    
    
    @IBAction func searchButtonTapped(sender: AnyObject) {
    }
    @IBAction func searchButtonPressed(sender: AnyObject) {
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        Utilities.runOnMainQueueAfterDelayInSeconds(3.0) {
            let appDelegate: AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
            let privateMoc: NSManagedObjectContext = appDelegate.coreDataStack.privateManagedObjectContext
            privateMoc.performBlock { () -> Void in
                ModelController().refreshArticles()
            }
        }
        
    }

    override func didReceiveMemoryWarning() {
        @IBOutlet weak var AppTItleLabel: UILabel!
        @IBOutlet weak var AppTitleLabel: UILabel!
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

