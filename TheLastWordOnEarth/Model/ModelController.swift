//
//  Created by Patrick Hogan on 21/10/2015.
//  Copyright © 2015 Patrick Hogan. All rights reserved.
//

import Foundation
import UIKit
import CoreData

class ModelController {

    let privateMoc: NSManagedObjectContext = {
        let appDelegate: AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        let moc: NSManagedObjectContext = appDelegate.coreDataStack.privateManagedObjectContext
        return moc
    }()
    
    func savePrivateContext() {
        let appDelegate: AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        appDelegate.coreDataStack.savePrivateContext()
    }
    
    func refreshArticles(callback: (() -> ())? = nil) {
        self.privateMoc.performBlock { () -> Void in
            LastWordService().retrieveArticlesSinceDate(self.dateOfMostRecentArticle(), processJson: { (json: NSDictionary) -> Void in
                if let headlines = json["headlines"] {
                    print("Article Count:",headlines.count)
                    for article in headlines as! NSArray {
                        self.processArticleDictionary(article as! NSDictionary)
                    }
                    self.savePrivateContext()
                }
                if let completionBlock = callback {
                    completionBlock()
                }
            })
        }
    }
    
    func dateOfMostRecentArticle() -> NSDate {
        var lastRefreshDate: NSDate = NSDate(dateString:"2013 Oct 28 16:24:00")
        let fetchRequest: NSFetchRequest = NSFetchRequest(entityName: "Article")
        fetchRequest.fetchLimit = 1
        let sortDescriptor = NSSortDescriptor(key: "timestamp", ascending: false)
        fetchRequest.sortDescriptors = [sortDescriptor]
        do {
            let results = try self.privateMoc.executeFetchRequest(fetchRequest)
            if (results.count > 0) {
                let latestArticle: Article = results.first as! Article
                lastRefreshDate = latestArticle.timestamp
            }
        } catch {
            print ("Error fetching timestamp")
        }
        return lastRefreshDate
    }

    func searchArticlesWithSearchTerm(searchTerm: String, processSearchResults:(searchResults: [SearchResult], forSearchTerm: String)->()) {
        LastWordService().retrieveArticlesMatchingSearchTerm(searchTerm, processJson: { (json: NSDictionary) -> Void in
            var searchResults: [SearchResult] = []
            if (json["searchResults"] != nil) {
                for searchResult in json["searchResults"] as! NSArray {
                    let resultArray: NSDictionary = searchResult as! NSDictionary
                    let dateString: String = resultArray["date"] as! String
                    let typeString: String = resultArray["type"] as! String
                    
                    let postDateDisplay = "Posted:" + NSDate(dateString: dateString).lastWordFormatDisplay()
                    let result: SearchResult = SearchResult(
                        postDate: NSDate(dateString: dateString),
                        film: String(resultArray["film"]!),
                        headline: String(resultArray["headline"]!),
                        smallImageUrl: String(resultArray["small_image"]!) + "=s200",
                        smallImage: nil,
                        type: Type(rawValue: typeString)!,
                        key: String(resultArray["key"]!),
                        postDateDisplay: postDateDisplay
                    )
                    searchResults.append(result)
                }
            }
            processSearchResults(searchResults: searchResults, forSearchTerm: searchTerm)
        })
    }
    
    func articleWithKey(key: String, processArticle: (articleID: NSManagedObjectID) -> ()) {

        //First we must see if this already exists in out database - if not we need to go to the network
        let article: Article? = self.articleWithKey(key)
        if article != nil  && article!.story != nil {
            self.privateMoc.refreshObject(article!, mergeChanges: true)
            processArticle(articleID: article!.objectID)
        } else {
        
            LastWordService().retrieveArticleWithKey(key, processJson: { (json: NSDictionary) -> Void in
                if (json["article"] != nil) {
                    let article: NSDictionary = json["article"] as! NSDictionary
                    let articleID = self.processArticleDictionary(article)
                    self.savePrivateContext()
                    processArticle(articleID: articleID)
                }
            })
        }
    }
    
    func articleWithObjectID(articleID: AnyObject?, processArticle: (article: Article) -> ()) {
        let artID: NSManagedObjectID = articleID as! NSManagedObjectID
        let art: Article = self.privateMoc.objectWithID(artID) as! Article
        processArticle(article: art)
    }
    
    func articleWithKey(key: String) -> Article? {
        var returnedArticle: Article? = nil
        let fetchRequest: NSFetchRequest = NSFetchRequest(entityName: "Article")
        fetchRequest.predicate = NSPredicate(format: "key = %@", key)
        do {
            let results = try self.privateMoc.executeFetchRequest(fetchRequest)
            if (results.count > 0) {
                returnedArticle = results.first as! Article?
            }
        } catch {
            print ("Error retrieving article with key")
        }
        return returnedArticle
    }

    func makeArticleFavourite(key: String) {
        let fetchRequest: NSFetchRequest = NSFetchRequest(entityName: "Article")
        fetchRequest.predicate = NSPredicate(format: "key = %@", key)
        var updatedArticle: Article?
        do {
            let results = try self.privateMoc.executeFetchRequest(fetchRequest)
            if results.count > 0 {
                updatedArticle = results.first as? Article
                updatedArticle?.favourite = true
                self.savePrivateContext()
                NSNotificationCenter.defaultCenter().postNotificationName(Constants.Notifications.favouritesChanged, object: nil)
            }
        } catch {
            print ("Failed to make article favourite")
        }
    }
    
    func removeArticleFavourite(key: String) {
        let fetchRequest: NSFetchRequest = NSFetchRequest(entityName: "Article")
        fetchRequest.predicate = NSPredicate(format: "key = %@", key)
        var updatedArticle: Article?
        do {
            let results = try self.privateMoc.executeFetchRequest(fetchRequest)
            if results.count > 0 {
                updatedArticle = results.first as? Article
                updatedArticle?.favourite = false
                self.savePrivateContext()
                NSNotificationCenter.defaultCenter().postNotificationName(Constants.Notifications.favouritesChanged, object: nil)
            }
        } catch {
            print ("Failed to make article favourite")
        }
    }

    func publishedDateFromString(dateString: String) -> NSDate {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy MMM dd HH:mm:ss"
        let published: NSDate = dateFormatter.dateFromString(dateString)!
        return published
    }
    
    func processArticleDictionary(article: NSDictionary) -> NSManagedObjectID {

        //Mandatory fields - in theory these could be nil so we should check rather than crash
        let key: String = article["key"] as! String
        let headline: String = article["headline"] as! String
        var type = Type.News
        if var typeString = article["type"] {
            if typeString as! String == "" {
                typeString = "News"
            }
            type = Type(rawValue: typeString.uppercaseString)!
        }
        let dateString = article["date"] as! String
        let published = self.publishedDateFromString(dateString)
        
        //Optional fields
        var smallImageUrl: String?
        if let smallUrl = article["small_image"] {
            smallImageUrl = String(smallUrl)
        }
        var largeImageUrl: String?
        if let largeUrl = article["large_image"] {
            largeImageUrl = String(largeUrl)
        }
        let subtitle = article["subtitle"] as! String?
        let story = article["story"] as! String?
        let imdbKey = article["imdb"] as! String?
        let trailer = article["trailer_url"] as! String?
        
        //See if this article exists already - if not create it
        var newArticle: Article? = self.articleWithKey(key)
        if newArticle == nil {
            let articleEntity = NSEntityDescription.entityForName("Article", inManagedObjectContext: self.privateMoc)
            newArticle = Article(entity: articleEntity!, insertIntoManagedObjectContext:self.privateMoc)
        }
        newArticle!.key = key
        newArticle!.timestamp = NSDate()
        newArticle!.headline = headline
        newArticle!.type = type.rawValue
        newArticle!.subtitle = subtitle
        newArticle!.story = story
        newArticle!.published = published
        newArticle!.smallImageUrl = smallImageUrl
        newArticle!.largeImageUrl = largeImageUrl
        newArticle!.imdbKey = imdbKey
        newArticle!.trailerUrl = trailer
        return newArticle!.objectID
    }
}

