//
//  Created by Patrick Hogan on 07/11/2015.
//  Copyright © 2015 Patrick Hogan. All rights reserved.
//

import Foundation

class ImageDownloadManager {
    
    var completionBlock: (ImageDownload) -> Void
    
    lazy var downloadQueue : NSOperationQueue = {
        let newDownloadQueue = NSOperationQueue()
        newDownloadQueue.name = "Image Download Queue"
        newDownloadQueue.maxConcurrentOperationCount = 10
        return newDownloadQueue
    }()
    
    lazy var downloadsInProgress : NSMutableDictionary = {
        let dic: NSMutableDictionary = NSMutableDictionary()
        return dic
    }()
    
    init?(completionBlock: (ImageDownload) -> Void) {
        self.completionBlock = completionBlock
    }
    
    func addUrl(url: NSURL, key: String) {
        do {
            let existingKey: AnyObject? = self.downloadsInProgress.objectForKey(url.absoluteString)
            if existingKey == nil {
                let imageDownload: ImageDownload = ImageDownload(url: url, key: key, completionBlock: { () in
                    self.imageDownloadDidFinish(url.absoluteString)
                })!
                self.downloadsInProgress.setObject(imageDownload, forKey: url.absoluteString)
                self.downloadQueue.addOperation(imageDownload)
            }
        }
    }
    
    func suspendQueue() {
        self.downloadQueue.suspended = true
    }
    
    func resumeQueue() {
        self.downloadQueue.suspended = false
    }
    
    func cancelAllOperations() {
        for imageDownload in self.downloadQueue.operations {
            imageDownload.cancel()
        }
        for entry in self.downloadsInProgress {
            let imageDownload = entry.value as! ImageDownload
            imageDownload.cancel()
        }
        self.downloadsInProgress = NSMutableDictionary()
    }
    
    func imageDownloadDidFinish(key: String) {
        //TODO: There is a nasty bug in here somewhere
        if let downloadEntry = self.downloadsInProgress.objectForKey(key) {
            let imageDownload: ImageDownload = downloadEntry as! ImageDownload
            self.downloadsInProgress.removeObjectForKey(key)
            if imageDownload.cancelled == false {
                self.completionBlock(imageDownload)
            }
        }
    }
    
}