//
//  TheLastWordOnEarth
//
//  Created by Patrick Hogan on 07/11/2015.
//  Copyright © 2015 Patrick Hogan. All rights reserved.
//

import Foundation
import UIKit

class ImageDownload: NSOperation {
    
    var url: NSURL! = nil
    var key: String! = nil
    var image: UIImage? = nil
    var failed: Bool = false
    
    init?(url: NSURL, key: String, completionBlock: () -> Void) {
        super.init()
        self.url = url
        self.key = key
        self.completionBlock = completionBlock
    }
    
    override func main() {
        if self.cancelled == false {
            var imageData: NSData? = NSData(contentsOfFile: self.fileName())
            if imageData == nil {
                imageData = NSData(contentsOfURL: self.url!)
                
                //TODO: Dont write to file if we didnt get an image back
                imageData?.writeToFile(self.fileName(), atomically: true)
            }
            if self.cancelled == false {
                if let data = imageData {
                    let downloadedImage: UIImage?  = UIImage(data: data)
                    if downloadedImage != nil {
                        self.image = downloadedImage
                    }
                } else {
                    self.failed = true
                }
            }
            if self.cancelled == false {
                self.completionBlock!()
            }
        }
    }
    
    func fileName() -> String {
        let applicationDocumentsDirectory: NSURL = NSFileManager.defaultManager().URLsForDirectory(NSSearchPathDirectory.DocumentDirectory, inDomains: NSSearchPathDomainMask.UserDomainMask).first!
        let path: String = (applicationDocumentsDirectory.path?.stringByAppendingString((self.url?.path)!))!
        return path
    }
}
