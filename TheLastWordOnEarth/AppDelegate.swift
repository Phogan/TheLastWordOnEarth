//
//  Created by Patrick Hogan on 08/10/2015.
//  Copyright © 2015 Patrick Hogan. All rights reserved.
//

import UIKit
import Fabric
import Crashlytics


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var appOpenedWithUrl = false
    lazy var coreDataStack = CoreDataStack()

    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        UIApplication.sharedApplication().statusBarStyle = .Default
        Fabric.with([Crashlytics.self])
        
        //TODO: do we need to logout (or something) to clear the session when the app closes ?
        let branch: Branch = Branch.getInstance()
        branch.initSessionWithLaunchOptions(launchOptions, andRegisterDeepLinkHandler: { params, error in
            if (error == nil) {
                let params: NSDictionary = params as NSDictionary
                let key: String? = params.objectForKey("key") as! String?
                if let safeKey = key {

                    //TODO: Look for a better way to do this
                    let rootVC: UIViewController = (self.window?.rootViewController)!
                    let newViewController = DetailViewController.init(articleKey: safeKey, nibName: "DetailViewController", bundle: nil)
                    rootVC.presentViewController(newViewController!, animated: true, completion: nil)
                }
            }
        })
        return true
    }

    func applicationWillResignActive(application: UIApplication) {
    }

    func applicationDidEnterBackground(application: UIApplication) {
        let defaults = NSUserDefaults.standardUserDefaults()
        defaults.setObject(NSDate(), forKey:"lastOpened")
        defaults.synchronize()
    }

    func applicationWillEnterForeground(application: UIApplication) {
    }

    func applicationDidBecomeActive(application: UIApplication) {
        let defaults = NSUserDefaults.standardUserDefaults()
        
        if let lastOpened = defaults.objectForKey("lastOpened") as! NSDate? {
            if lastOpened.timeIntervalSinceNow * -1 > 60 * 60 {
                ModelController().refreshArticles()
            }
        }

    }

    func applicationWillTerminate(application: UIApplication) {
        self.coreDataStack.saveMainContext()
    }
    
    func application(app: UIApplication, openURL url: NSURL, options: [String : AnyObject]) -> Bool {
        self.appOpenedWithUrl = true
        let branch: Branch = Branch.getInstance()
        branch.handleDeepLink(url)
        return true
    }
}

