//
//  Created by Patrick Hogan on 16/10/2015.
//  Copyright © 2015 Patrick Hogan. All rights reserved.
//

import Foundation
import UIKit

struct Constants {
    
    struct Menu {
        struct Home {
            static let label = "Latest"
            static let imageName = "latest_menu"
            static let viewName = "homeViewController"
            static let selected = true
        }
        struct News {
            static let label = "News"
            static let imageName = "News"
            static let viewName = "newsViewController"
            static let selected = false
        }
        struct Reviews {
            static let label = "Reviews"
            static let imageName = "reviews_menu"
            static let viewName = "reviewsViewController"
            static let selected = false
        }
        struct Features {
            static let label = "Songs"
            static let imageName = "feature_menu"
            static let viewName = "featuresViewController"
            static let selected = false
        }
        struct Trailers {
            static let label = "Trailers"
            static let imageName = "Trailers"
            static let viewName = "trailersViewController"
            static let selected = false
        }
        struct Favourites {
            static let label = "Saved"
            static let imageName = "favourite_menu"
            static let viewName = "favouritesViewController"
            static let selected = false
        }
        struct Bafta {
            static let label = "Bafta"
            static let imageName = "Bafta"
            static let viewName = "baftaViewController"
            static let selected = false
        }
    }
    
    static let firstCellViewName = "firstCell"
    static let searchViewName = "searchViewController"
    static let numArticlesInFirstCell = 6

    static let bannerHeight: CGFloat = Utilities.compactWidth() ? 50.0 : 60.0
    static let statusBarHeight: CGFloat = 20.0
    static let refreshPanelHeight: CGFloat = Utilities.compactWidth() ? 150.0 : 150.0

    static let spinnerLineWidth: CGFloat = 15
    static let spinnerDiameterOuter: CGFloat = 60
    static let spinnerDiameterInner: CGFloat = 40
    static let spinnerDiameterVeryInner: CGFloat = 30

    static let MenuButtonMap: [(label: String, imageName: String, viewName: String, selected: Bool)] = [
        (label: Menu.Home.label, imageName: Menu.Home.imageName, viewName: Menu.Home.viewName, selected: Menu.Home.selected),
        (label: Menu.Reviews.label, imageName: Menu.Reviews.imageName, viewName: Menu.Reviews.viewName, selected: Menu.Reviews.selected),
        (label: Menu.Features.label, imageName: Menu.Features.imageName, viewName: Menu.Features.viewName, selected: Menu.Features.selected),
        (label: Menu.Favourites.label, imageName: Menu.Favourites.imageName, viewName: Menu.Favourites.viewName, selected: Menu.Favourites.selected),
    ]
    
    struct CellIdentifier {
        static let Menu = "MenuCollectionViewCell"
    }
    
    struct CellSize {
        struct Menu {
            static let normalHeight = 100
            static let compactWidth = 80
        }
        struct SearchResult {
            static let normalHeight = 160
            static let compactHeight = 100
        }
        struct FirstCell {
            static let normalHeight: CGFloat = 380
            static let normalHeightLandscape: CGFloat = 320
            static let compactHeight: CGFloat = 300
            static let compactHeightLandscape: CGFloat = 200
        }
    }
    
    struct SectionInset {
        struct FirstCell {
            static let sectionInsetPortraitCompact = UIEdgeInsetsMake(0,0,0,0)
            static let sectionInsetLandscapeCompact = UIEdgeInsetsMake(0,0,0,0)
        }
    }
    
    struct Colors {
        static let mainTheme = UIColor(red:0 / 255, green: 155 / 255, blue: 255 / 255, alpha: 1.00)
        static let newsIcon = UIColor.redColor().colorWithAlphaComponent(0.4)
        static let reviewIcon = mainTheme.colorWithAlphaComponent(0.4)
        static let songIcon = UIColor.greenColor().colorWithAlphaComponent(0.4)
        static let otherIcon = UIColor.yellowColor().colorWithAlphaComponent(0.4)
        static let spinnerOne = Constants.Colors.mainTheme
        static let spinnerTwo = Constants.Colors.mainTheme.colorWithAlphaComponent(0.5)
    }

    struct AnimationDuration {
        static let pageSwitch = 0.4
    }

    struct Notifications {
        static let searchCencelled = "SEARCH_CANCELLED_NOTIFICATION"
        static let deleteCencelled = "DELETE_CANCELLED_NOTIFICATION"
        static let favouritesChanged = "FAVOURITES_CHANGED_NOTIFICATION"
    }

    struct Urls {
        static let latestArticles = "http://www.last-word-on-earth.appspot.com/lw.news.service2.topnews"
        static let searchRequest = "http://www.last-word-on-earth.appspot.com/lw.news.service2.search"
        static let singleArticleRequest = "http://www.last-word-on-earth.appspot.com/lw.news.service2.article"
        static let disqusPosts = "http://disqus.com/api/3.0/forums/listPosts.json"
    }
    
    static let PageCollectionSettings: [String : CollectionConfig] = [
        Constants.Menu.Home.viewName : Constants.latestConfig(),
        Constants.Menu.Reviews.viewName : Constants.reviewsConfig(),
        Constants.Menu.Features.viewName : Constants.featuresConfig(),
        Constants.firstCellViewName : Constants.firstCellConfig(),
        Constants.Menu.Favourites.viewName : Constants.favouritesConfig(),
    ]
    
    static func latestConfig() -> CollectionConfig {
        var config = CollectionConfig()
        config.widthMultiplierNormal = 4
        config.widthMultiplierCompact = 1
        config.heightNormal = 300
        config.heightCompact = 140
        config.heightNormalLandscape = 300
        config.heightCompactLandscape = 160
        config.sectionInsetPortraitCompact = UIEdgeInsetsMake(70,0,0,0)
        config.sectionInsetLandscapeCompact = UIEdgeInsetsMake(70,0,0,0)
        config.sectionInsetPortraitNormal = UIEdgeInsetsMake(40,0,0,0)
        config.sectionInsetLandscapeNormal = UIEdgeInsetsMake(40,0,0,0)
        return config
    }

    static func reviewsConfig() -> CollectionConfig {
        var config = CollectionConfig()
        config.widthMultiplierNormal = 2
        config.widthMultiplierCompact = 1
        config.heightNormal = 320
        config.heightCompact = 220
        config.heightNormalLandscape = 250
        config.heightCompactLandscape = 250
        config.sectionInsetPortraitCompact = UIEdgeInsetsMake(70,0,0,0)
        config.sectionInsetLandscapeCompact = UIEdgeInsetsMake(70,0,0,0)
        config.sectionInsetPortraitNormal = UIEdgeInsetsMake(40,0,0,0)
        config.sectionInsetLandscapeNormal = UIEdgeInsetsMake(40,0,0,0)
        return config
    }

    static func featuresConfig() -> CollectionConfig {
        var config = CollectionConfig()
        config.widthMultiplierCompact = 2
        config.widthMultiplierNormal = 3
        config.heightNormal = 360
        config.heightCompact = 180
        config.heightNormalLandscape = 440
        config.heightCompactLandscape = 220
        config.sectionInsetPortraitCompact = UIEdgeInsetsMake(90,20,0,20)
        config.sectionInsetLandscapeCompact = UIEdgeInsetsMake(90,20,0,20)
        config.sectionInsetPortraitNormal = UIEdgeInsetsMake(60,20,0,20)
        config.sectionInsetLandscapeNormal = UIEdgeInsetsMake(60,20,0,20)
        return config
    }

    static func firstCellConfig() -> CollectionConfig {
        var config = CollectionConfig()
        config.heightMultiplierCompact = 1
        config.heightMultiplierNormal = 1
        
        config.widthNormal = 300
        config.widthCompact = 240
        config.widthNormalLandscape = 400
        config.widthCompactLandscape = 240
        config.sectionInsetPortraitCompact = UIEdgeInsetsMake(0,0,0,0)
        config.sectionInsetLandscapeCompact = UIEdgeInsetsMake(0,0,0,0)
        config.sectionInsetPortraitNormal = UIEdgeInsetsMake(0,0,0,0)
        config.sectionInsetLandscapeNormal = UIEdgeInsetsMake(0,0,0,0)
        return config
    }

    static func favouritesConfig() -> CollectionConfig {
        var config = CollectionConfig()
        config.widthMultiplierNormal = 1
        config.widthMultiplierCompact = 1
        config.heightNormal = 220
        config.heightCompact = 100
        config.heightNormalLandscape = 220
        config.heightCompactLandscape = 160
        config.sectionInsetPortraitCompact = UIEdgeInsetsMake(70,0,0,0)
        config.sectionInsetLandscapeCompact = UIEdgeInsetsMake(70,0,0,0)
        config.sectionInsetPortraitNormal = UIEdgeInsetsMake(40,0,0,0)
        config.sectionInsetLandscapeNormal = UIEdgeInsetsMake(40,0,0,0)
        return config
    }

}






