//
//  Created by Patrick Hogan on 02/12/2015.
//  Copyright © 2015 Patrick Hogan. All rights reserved.
//

import Foundation
import UIKit

struct CollectionConfig {

    var sectionInsetPortraitCompact: UIEdgeInsets!
    var sectionInsetLandscapeCompact: UIEdgeInsets!
    var sectionInsetPortraitNormal: UIEdgeInsets!
    var sectionInsetLandscapeNormal: UIEdgeInsets!

    var widthMultiplierCompact: Int = 0
    var widthMultiplierNormal: Int = 0
    var heightMultiplierCompact: Int = 0
    var heightMultiplierNormal: Int = 0

    var widthNormal: CGFloat = 0.0
    var widthNormalLandscape: CGFloat = 0.0
    var widthCompact: CGFloat = 0.0
    var widthCompactLandscape: CGFloat = 0.0

    var heightNormal: CGFloat = 0.0
    var heightNormalLandscape: CGFloat = 0.0
    var heightCompact: CGFloat = 0.0
    var heightCompactLandscape: CGFloat = 0.0

}
