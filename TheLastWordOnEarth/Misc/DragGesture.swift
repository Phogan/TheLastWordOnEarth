//
//  DragGesture.swift
//  TheLastWordOnEarth
//
//  Created by Patrick Hogan on 04/12/2015.
//  Copyright © 2015 Patrick Hogan. All rights reserved.
//

import Foundation

class Drag {
    
    static var vw: UIView!
    static var tg: AnyObject? = nil
    
    class func addGesturesToView(view: UIView, target: AnyObject) {
        self.vw = view
        self.tg = target
        self.addGesturesGestures()
    }

    class func addGesturesGestures() {
        self.addSwipeGesturesForDirection(.Left)
        self.addSwipeGesturesForDirection(.Right)
    }
    
    class func addSwipeGesturesForDirection(direction: UISwipeGestureRecognizerDirection) {
        let swipeGesture = UISwipeGestureRecognizer()
        swipeGesture.direction = direction
        swipeGesture.addTarget(self.tg!, action: "swipe:")
        self.vw.addGestureRecognizer(swipeGesture)
    }


}