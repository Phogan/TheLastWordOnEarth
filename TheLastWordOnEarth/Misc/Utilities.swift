//
//  TheLastWordOnEarth
//
//  Created by Patrick Hogan on 21/10/2015.
//  Copyright © 2015 Patrick Hogan. All rights reserved.
//

import Foundation
import UIKit

class Utilities {

    class func runOnMainQueueAfterDelayInSeconds(delay:Double, codeBlock:()->()) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW,Int64(delay * Double(NSEC_PER_SEC))),dispatch_get_main_queue(), codeBlock)
    }
 
    class func runOnMainQueueImmediately(codeBlock:()->()) {
        dispatch_async(dispatch_get_main_queue(), codeBlock)
    }

    class func blurView(view: UIView, sendToBack: Bool = false) -> UIView {
        let blur = UIBlurEffect.init(style: .Dark)
        let effectView = UIVisualEffectView.init(effect: blur)
        effectView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(effectView)
        if sendToBack {
            view.sendSubviewToBack(effectView)
        } else {
            view.bringSubviewToFront(effectView)
        }
        addConstraintsForParentView(view, childView: effectView)
        effectView.tag = 99
        return effectView
    }
    
    class func unBlurView(view: UIView?) {
        if let parentView = view {
            for subView in parentView.subviews {
                if (subView.tag == 99) {
                    subView.removeFromSuperview()
                }
            }
        }
    }
    
    class func addGradientToView(view: UIView, startColor: CGColor, endColor: CGColor, locations: [NSNumber]) -> CAGradientLayer {
        let gradientLayer: CAGradientLayer = CAGradientLayer()
        gradientLayer.colors = [startColor, endColor]
        gradientLayer.locations = locations
        gradientLayer.frame = view.bounds
        view.layer.insertSublayer(gradientLayer, atIndex: 0)
        return gradientLayer
    }
    
    class func addConstraintsForParentView(parentView: UIView, childView: UIView) {
        var constraints = [NSLayoutConstraint]()
        constraints.append(NSLayoutConstraint(item: childView,attribute: .Height, relatedBy: .Equal, toItem: parentView,attribute: .Height, multiplier: 1, constant: 0))
        constraints.append(NSLayoutConstraint(item: childView,attribute: .Width, relatedBy: .Equal, toItem: parentView,attribute: .Width, multiplier: 1, constant: 0))
        constraints.append(NSLayoutConstraint(item: childView,attribute: .Top, relatedBy: .Equal, toItem: parentView,attribute: .Top, multiplier: 1, constant: 0))
        constraints.append(NSLayoutConstraint(item: childView,attribute: .Left, relatedBy: .Equal, toItem: parentView,attribute: .Left, multiplier: 1, constant: 0))
        parentView.addConstraints(constraints)
    }
    
    class func addSpinnerToView(view: UIView, radius: CGFloat, clockwise: Bool) -> Spinner {
        let spinnerFrame = CGRectMake(view.center.x - radius, view.center.y - radius, radius * 2, radius * 2)
        let newSpinner: Spinner = Spinner(frame: spinnerFrame, radius: radius, lineWidth: Constants.spinnerLineWidth, clockwise: clockwise)
        newSpinner.backgroundColor = UIColor.clearColor()
        view.addSubview(newSpinner)
        view.bringSubviewToFront(newSpinner)
        return newSpinner
    }
    
    class func showSpinnerInView(view: UIView) {
        Utilities.hideSpinnerInView(view)

        let frameWidth = Constants.spinnerDiameterOuter * 1.5
        let spinnerFrame: UIView = UIView(frame: CGRectMake(view.center.x - frameWidth / 2, view.center.y - frameWidth / 2, frameWidth, frameWidth))
        spinnerFrame.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.5)
        spinnerFrame.layer.cornerRadius = 5.0
        spinnerFrame.tag = 999
        view.addSubview(spinnerFrame)
        view.bringSubviewToFront(spinnerFrame)
        addConstraintsForParentView(view, childView: spinnerFrame)
        
        let spinnerOuter = Utilities.addSpinnerToView(view, radius: Constants.spinnerDiameterOuter / 2, clockwise: true)
        let spinnerInner = Utilities.addSpinnerToView(view, radius: Constants.spinnerDiameterInner / 2, clockwise: true)
        rotateSpinner(spinnerOuter, clockwise: true, speed: 0.40)
        rotateSpinner(spinnerInner, clockwise: false, speed: 0.30)
    }

    class func hideSpinnerInView(view: UIView) {
        for subView in view.subviews {
            if subView.isKindOfClass(Spinner.self) || subView.tag == 999 {
                subView.tag = 888
                subView.removeFromSuperview()
            }
        }
    }
    
    class func rotateSpinner(view: UIView, clockwise: Bool, speed: Double) {
        let angle: CGFloat = clockwise ? CGFloat(M_PI_2) : CGFloat(M_PI_2 * -1)
        UIView.animateWithDuration(speed, delay: 0, options: .CurveLinear, animations: {
            view.transform = CGAffineTransformRotate(view.transform, angle)
            }, completion: { finished in
                if view.tag != 888 {
                    self.rotateSpinner(view, clockwise: clockwise, speed: speed)
                }
        })
    }
    
    class func currentTimeMillis() -> Int64{
        let nowDouble = NSDate().timeIntervalSince1970
        return Int64(nowDouble*1000)
    }
    
    class func displayUrlInWebViewController(urlString: String, presentingViewController: UIViewController) {
        let webViewController = WebViewController.init(urlString: urlString, presentingViewController: presentingViewController)
        webViewController?.display()
    }
    
    class func toggleFavourite(key: String) {
        ModelController().makeArticleFavourite(key)
        Utilities.displayConfirmation()
    }

    class func landscape() -> Bool {
        if UIDevice.currentDevice().orientation == .LandscapeLeft || UIDevice.currentDevice().orientation == .LandscapeRight {
            return true
        } else {
            return false
        }
    }

    class func compactWidth() -> Bool {
        let rootViewController: UIViewController = (UIApplication.sharedApplication().windows.first?.rootViewController)!
        if rootViewController.traitCollection.horizontalSizeClass == UIUserInterfaceSizeClass.Compact {
            return true
        } else {
            return false
        }
    }
    
    class func displayConfirmation() {
        if let mainWindow = UIApplication.sharedApplication().windows.first {
            let newView = UIView(frame: CGRectMake(0, 0, 100, 100))
            newView.backgroundColor = UIColor.blackColor()
            newView.alpha = 0.7
            newView.center = mainWindow.center
            newView.layer.cornerRadius = 3.0
            newView.tag = 777
            mainWindow.addSubview(newView)
            mainWindow.bringSubviewToFront(newView)
            let lineLayer1 = CAShapeLayer()
            lineLayer1.frame = newView.bounds
            lineLayer1.strokeColor = Constants.Colors.mainTheme.CGColor
            lineLayer1.lineWidth = 10
            lineLayer1.strokeStart = 0.0
            let path = Utilities.getPathForTick()
            lineLayer1.path = path.CGPath
            newView.layer.addSublayer(lineLayer1)
            let basicAnimation = CABasicAnimation(keyPath: "strokeEnd")
            basicAnimation.fromValue = 0.0
            basicAnimation.toValue = 1.0
            basicAnimation.duration = 0.5
            lineLayer1.addAnimation(basicAnimation, forKey: "strokeEndAnimation")
            Utilities.runOnMainQueueAfterDelayInSeconds(1.0) {
                UIView.animateWithDuration(1.0, delay: 0.0, options: .CurveEaseOut,
                    animations: {
                        newView.alpha = 0.0
                    },
                    completion: { (finished) in
                        newView.removeFromSuperview()
                    }
                )
            }
        }
    }
    
    class func getPathForTick() -> UIBezierPath {
        let path = UIBezierPath()
        path.moveToPoint(CGPointMake(20,60))
        path.addLineToPoint(CGPointMake(40,80))
        path.addLineToPoint(CGPointMake(80,20))
        return path
    }
    
    class func createPathFromText(text: String, pathPoint: CGPoint) -> UIBezierPath {
        let letters = CGPathCreateMutable()
        let font = CTFontCreateWithName("Wingdings", 120, nil)
        let attrs = [String(kCTFontAttributeName): font]
        let attrString = NSAttributedString(string: text, attributes: attrs)
        let line = CTLineCreateWithAttributedString(attrString)
        let runArray = CTLineGetGlyphRuns(line)
        let run: CTRun = unsafeBitCast(CFArrayGetValueAtIndex(runArray, 0),CTRun.self)
        let runFont = font
        for runGlyphIndex in 0 ..< CTRunGetGlyphCount(run){
            let thisGlyphRange = CFRangeMake(runGlyphIndex, 1)
            var glyph = CGGlyph()
            var position = CGPointZero
            CTRunGetGlyphs(run, thisGlyphRange, &glyph)
            CTRunGetPositions(run, thisGlyphRange, &position)
            let letter = CTFontCreatePathForGlyph(runFont, glyph, nil)
            var t = CGAffineTransformMakeTranslation(position.x, position.y)
            CGPathAddPath(letters, &t, letter);
        }
        
        let path = UIBezierPath()
        path.moveToPoint(pathPoint)
        path.appendPath(UIBezierPath(CGPath: letters))
        return path
    }
}