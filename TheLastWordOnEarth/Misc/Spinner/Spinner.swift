//
//  Created by Patrick Hogan on 17/11/2015.
//  Copyright © 2015 Patrick Hogan. All rights reserved.
//

import Foundation
import UIKit

class Spinner: UIView {
    
    var lineWidth: CGFloat!
    var radius: CGFloat!
    var clockwise: Bool!
    
    convenience init(frame: CGRect, radius: CGFloat, lineWidth: CGFloat, clockwise: Bool) {
        self.init(frame: frame)
        self.lineWidth = lineWidth
        self.radius = radius
        self.clockwise = clockwise
    }
    
    override func drawRect(rect: CGRect) {
        
        let innerRadius = (self.radius * 2 - self.lineWidth) / 2
        let context = UIGraphicsGetCurrentContext()

        let bezierPath = UIBezierPath()
        bezierPath.usesEvenOddFillRule = true
        bezierPath.moveToPoint(CGPointMake(self.radius * 2, self.radius))
        bezierPath.addArcWithCenter(CGPointMake(self.radius, self.radius), radius: self.radius, startAngle: 0, endAngle: CGFloat(2.0) * CGFloat(M_PI) , clockwise: self.clockwise)
        
        let shapeMask = CAShapeLayer()
        shapeMask.frame = self.bounds
        shapeMask.path = bezierPath.CGPath

        let bezierPathInner = UIBezierPath()
        bezierPathInner.moveToPoint(CGPointMake(self.radius * 2, self.radius))
        bezierPathInner.addArcWithCenter(CGPointMake(self.radius, self.radius), radius: innerRadius, startAngle: 0, endAngle: CGFloat(2.0) * CGFloat(M_PI) , clockwise: self.clockwise)
        bezierPath.appendPath(bezierPathInner)

        CGContextSaveGState(context)
        bezierPath.addClip()

        let colors: CFArray? =  [Constants.Colors.spinnerOne.CGColor, Constants.Colors.spinnerTwo.CGColor]
        let gradient = CGGradientCreateWithColors(CGColorSpaceCreateDeviceRGB(), colors, [0.38, 0.73, 1])!
        CGContextDrawLinearGradient(context, gradient, CGPointMake(self.radius, 0), CGPointMake(self.radius, self.radius * 2), CGGradientDrawingOptions())
        
        CGContextRestoreGState(context)
        UIColor.blackColor().setStroke()
        bezierPath.lineWidth = 0.25
        bezierPath.stroke()
        
    }
}
