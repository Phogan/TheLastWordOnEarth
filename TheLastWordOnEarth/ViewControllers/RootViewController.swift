//
//  Created by Patrick Hogan on 11/10/2015.
//  Copyright © 2015 Patrick Hogan. All rights reserved.
//

import Foundation
import UIKit
import Crashlytics

enum ViewPresentationDirection: Int {
    case None = 0
    case Left = 1
    case Right = 2
    case Top = 3
    case Bottom = 4
}


class RootViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var mainContainer: UIView!
    @IBOutlet weak var menuCollectionView: UICollectionView!
    @IBOutlet weak var menuContainer: UIView!
    @IBOutlet weak var searchButton: UIButton!
    @IBOutlet weak var searchTextButton: UIButton!
    @IBOutlet weak var bannerView: UIView!
    @IBOutlet weak var appTitleLabel: UILabel!
    @IBOutlet weak var bannerHeight: NSLayoutConstraint!

    @IBAction func searchButtonTapped(sender: AnyObject) {
        if searchVisible == false {
            self.displaySearch()
        } else {
            self.dismissSearch()
        }
    }
    
    var searchVisible = false
    var currentViewController: UIViewController? = nil
    var menuButtonMap = Constants.MenuButtonMap
    var currentButtonIndex: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.bannerView.backgroundColor = Constants.Colors.mainTheme
        self.menuCollectionView.backgroundColor = UIColor.clearColor()
        
        //As the main view controller loads we will kick off a background task to fetch recent articles
        Utilities.showSpinnerInView(self.view)
        ModelController().refreshArticles() {
            Utilities.runOnMainQueueAfterDelayInSeconds(0.0) {
                Utilities.hideSpinnerInView(self.view)
            }
        }
        
        //Setup the menu
        self.menuCollectionView.registerNib(UINib.init(nibName: Constants.CellIdentifier.Menu, bundle: nil), forCellWithReuseIdentifier: Constants.CellIdentifier.Menu)
        self.menuCollectionView.dataSource = self
        self.menuCollectionView.delegate = self
        self.menuCollectionView.reloadData()

        //Show the first item in the menu map that is set to selected
        for index in 0..<Constants.MenuButtonMap.count {
            if Constants.MenuButtonMap[index].selected == true {
                showViewControllerForMenuIndex(index, direction: .None)
                currentButtonIndex = index
                break
            }
        }
        Drag.addGesturesToView(self.view, target: self)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func traitCollectionDidChange(previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        let flowLayout: UICollectionViewFlowLayout = self.menuCollectionView.collectionViewLayout as! UICollectionViewFlowLayout
        if (self.traitCollection.horizontalSizeClass == UIUserInterfaceSizeClass.Compact) {
            flowLayout.scrollDirection = UICollectionViewScrollDirection.Horizontal
            self.menuCollectionView!.alwaysBounceHorizontal = true
            self.menuCollectionView!.alwaysBounceVertical = false
        } else {
            flowLayout.scrollDirection = UICollectionViewScrollDirection.Vertical
            self.menuCollectionView!.alwaysBounceHorizontal = false
            self.menuCollectionView!.alwaysBounceVertical = true
        }
        self.menuCollectionView!.directionalLockEnabled = true
        self.menuCollectionView!.reloadData()
        
    }

    // MARK: -
    
    // MARK: Menu Collection View methods
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {

        let cellWidth = (self.traitCollection.horizontalSizeClass == UIUserInterfaceSizeClass.Compact) ?
            
            self.widthForCellAtIndexPath(indexPath)
            
            : CGFloat(self.menuCollectionView.frame.size.width)
        
        let cellHeight = (self.traitCollection.horizontalSizeClass == UIUserInterfaceSizeClass.Compact) ? CGFloat(self.menuCollectionView.frame.size.height) : CGFloat(Constants.CellSize.Menu.normalHeight)
        return CGSizeMake(cellWidth,cellHeight)
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return menuButtonMap.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(Constants.CellIdentifier.Menu, forIndexPath: indexPath) as! MenuCollectionViewCell
        let menuItem:(label: String, imageName: String, viewName: String, selected: Bool) = self.menuButtonMap[indexPath.item]
        cell.menuLabel.text = menuItem.label
        cell.menuImage.image = UIImage.init(named:menuItem.imageName)
        
        //We need to pass in the trait collection because in the view it will not be correctly assigned during initialisation
        cell.configureCell(menuItem.selected, traitCollection: self.traitCollection)
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        if self.currentButtonIndex < indexPath.item {
            self.showMenuItemAtIndexPath(indexPath, direction: .Left)
        } else if self.currentButtonIndex > indexPath.item {
            self.showMenuItemAtIndexPath(indexPath, direction: .Right)
        }
    }

    // MARK: -

    // MARK: Navigation methods - Move into a separate class if this starts to grow!

    func showMenuItemAtIndexPath(indexPath: NSIndexPath, direction: ViewPresentationDirection) {

        //Update the map to deselect all menu items - then select the new one
        for index in 0..<self.menuButtonMap.count {
            let menuItem = self.menuButtonMap[index]
            self.menuButtonMap[index] = (label: menuItem.label, imageName: menuItem.imageName, viewName: menuItem.viewName, selected: false)
        }
        let menuItem = self.menuButtonMap[indexPath.item]
        self.menuButtonMap[indexPath.item] = (label: menuItem.label, imageName: menuItem.imageName, viewName: menuItem.viewName, selected: true)
        self.menuCollectionView.reloadData()
        self.menuCollectionView.scrollToItemAtIndexPath(indexPath, atScrollPosition: UICollectionViewScrollPosition.CenteredHorizontally, animated: true)
        self.showViewControllerForMenuIndex(indexPath.item, direction: direction)
        self.currentButtonIndex = indexPath.item
    }
    
    func showViewControllerForMenuIndex(index: Int, direction: ViewPresentationDirection) {
        self.dismissSearch()
        self.showViewControllerForMenuItem(self.menuButtonMap[index].viewName, direction: direction)
    }
    
    func showViewControllerForMenuItem(viewName: String, direction: ViewPresentationDirection) -> UIViewController? {
        
        var newViewController: UIViewController!
    
        //Fist see if there is already a child view controller of this type instantiated
        for childViewController in self.childViewControllers {
            if (childViewController.restorationIdentifier! == viewName) {
                //TODO: Sort this out...Lets take search out of here as it doesnt fit the pattern
                if viewName == Constants.searchViewName {
                    newViewController = childViewController as! SearchViewController
                } else {
                    newViewController = childViewController as! CollectionViewController
                }
            }
        }
        
        //If the current viewcontroller is the same as the new one dont do anything
        if (self.currentViewController == nil || newViewController != self.currentViewController) {
            if newViewController == nil {

                //TODO: Sort this out...Lets take search out of here as it doesnt fit the pattern
                if viewName == Constants.searchViewName {
                    newViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier(viewName) as! SearchViewController
                } else {
                    newViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier(viewName) as! CollectionViewController
                }
                newViewController.title = viewName
                self.addChildViewController(newViewController!)
            }
            newViewController.willMoveToParentViewController(self)
            self.mainContainer.addSubview(newViewController.view)
            
            //Animate the presentation
            let endFrame: CGRect = self.mainContainer.bounds
            var startFrame: CGRect = endFrame
            
            //Alter the start frame based on the direction
            switch direction {
            case .Left:
                startFrame.origin.x += endFrame.size.width
            case .Right:
                startFrame.origin.x -= endFrame.size.width
            case .Top:
                startFrame.origin.y -= endFrame.size.height
            case .Bottom:
                startFrame.origin.y += endFrame.size.height
            default: break
            }
            
            newViewController.view.frame = startFrame
            newViewController.view.alpha = 1.0
            UIView.animateWithDuration(Constants.AnimationDuration.pageSwitch, delay: 0.0, options: .CurveEaseIn, animations: {
                newViewController.view.frame = endFrame
                newViewController.view.alpha = 1.0
                self.bannerHeight.constant = Constants.bannerHeight
                self.appTitleLabel.alpha = 1.0
                self.searchButton.alpha = 1.0
                self.searchTextButton.alpha = 1.0
                }, completion: { (b: Bool) in
                })
            
            //TODO: Sort this out...
            if viewName != Constants.searchViewName {
                self.currentViewController = newViewController!
            }
            
            newViewController!.didMoveToParentViewController(self)
        }
        return newViewController
    }
    
    func displaySearch() {
        
        //First blur the current view - by this poinr
        if searchVisible == false {
            
            NSNotificationCenter.defaultCenter().addObserverForName(Constants.Notifications.searchCencelled, object: nil, queue: nil, usingBlock: { (NSNotification) in
                self.dismissSearch()
            })
            
            Utilities.blurView(self.currentViewController!.view)
            if let newViewController = self.showViewControllerForMenuItem(Constants.searchViewName, direction: .Top) {
                newViewController.view.backgroundColor = UIColor.clearColor()
                self.view.bringSubviewToFront(newViewController.view)
            }
            searchVisible = true
            self.searchButton.setImage(UIImage(named: "close"), forState: .Normal)
            self.searchTextButton.setTitle("Close", forState: .Normal)
        }
    }
    
    func dismissSearch() {
        if searchVisible == true {
            NSNotificationCenter.defaultCenter().removeObserver(self)
            Utilities.unBlurView(self.currentViewController?.view)
            for childViewController in self.childViewControllers {
                if (childViewController.restorationIdentifier! == Constants.searchViewName) {
                    childViewController.view.removeFromSuperview()
                }
            }
            searchVisible = false
            self.searchButton.setImage(UIImage(named: "search"), forState: .Normal)
            self.searchTextButton.setTitle("Search", forState: .Normal)
        }
    }
    
    func swipe(recogniser: UISwipeGestureRecognizer) {
        let direction: ViewPresentationDirection
        let currentIndex = self.currentButtonIndex
        if recogniser.direction == .Left {
            direction = .Left
            self.currentButtonIndex = min(self.currentButtonIndex + 1, self.menuButtonMap.count - 1)
        } else {
            direction = .Right
            self.currentButtonIndex = max(self.currentButtonIndex - 1, 0)
        }
        if currentIndex != self.currentButtonIndex {
            let indexPath: NSIndexPath = NSIndexPath(forItem: self.currentButtonIndex, inSection: 0)
            self.menuCollectionView.selectItemAtIndexPath(indexPath, animated: true, scrollPosition: .Left)
            self.showMenuItemAtIndexPath(indexPath,direction: direction)
        }
    }
    
    func widthForCellAtIndexPath(indexPath: NSIndexPath) -> CGFloat {
        var textWidth: CGFloat = 0.0
        let menuText = self.menuButtonMap[indexPath.item].label
        let menuFont = UIFont(name: "Helvetica Neue", size: 18.0)
        if let ns_str:NSString = menuText as NSString? {
            let sizeOfString = ns_str.boundingRectWithSize(
                CGSizeMake(CGFloat(Constants.CellSize.Menu.compactWidth), CGFloat.infinity),
                options: NSStringDrawingOptions.UsesLineFragmentOrigin,
                attributes: [NSFontAttributeName: menuFont!],
                context: nil).size
            textWidth = sizeOfString.width
        }
        return textWidth + 30
    }
}

