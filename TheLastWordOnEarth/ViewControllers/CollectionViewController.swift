//
//  Created by Patrick Hogan on 12/10/2015.
//  Copyright © 2015 Patrick Hogan. All rights reserved.
//

import UIKit
import CoreData
import SafariServices

class CollectionViewController: UICollectionViewController, NSFetchedResultsControllerDelegate {
    
    lazy var fetchedResultController: NSFetchedResultsController! = nil
    var articles: [Article] = []
    var Delegate: FirstCellCollectionViewDelegate! = nil
    
    var refreshPanel: UILabel!
    var refreshingArticles: Bool = false
    
    //TODO: We may need to revise this map as a one to many
    var identifierToTypeMap: NSDictionary = [
        "newsViewController" : "type = 'NEWS'",
        "reviewsViewController" : "type = 'REVIEW'",
        "featuresViewController" : "type IN {'SONG','SONGYEAR'}",
        "trailersViewController" : "type = 'TRAILER'",
        "favouritesViewController" : "favourite = true",
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
                
        //TODO: These should be registered as required - we dont always need them all
        self.collectionView?.registerNib(UINib.init(nibName: "FirstCell", bundle: nil), forCellWithReuseIdentifier: "FirstCell")
        self.collectionView?.registerNib(UINib.init(nibName: "NewsCellNormal", bundle: nil), forCellWithReuseIdentifier: "NEWSCellNormal")
        self.collectionView?.registerNib(UINib.init(nibName: "FavouriteCellCompact", bundle: nil), forCellWithReuseIdentifier: "FavouriteCellCompact")
        self.collectionView?.registerNib(UINib.init(nibName: "FavouriteCellNormal", bundle: nil), forCellWithReuseIdentifier: "FavouriteCellNormal")
        self.collectionView?.registerNib(UINib.init(nibName: "ReviewCellNormal", bundle: nil), forCellWithReuseIdentifier: "REVIEWCellNormal")
        self.collectionView?.registerNib(UINib.init(nibName: "ReviewCellCompact", bundle: nil), forCellWithReuseIdentifier: "REVIEWCellCompact")
        self.collectionView?.registerNib(UINib.init(nibName: "LatestCellCompact", bundle: nil), forCellWithReuseIdentifier: "LatestCellCompact")
        self.collectionView?.registerNib(UINib.init(nibName: "LatestCellNormal", bundle: nil), forCellWithReuseIdentifier: "LatestCellNormal")
        self.collectionView?.registerNib(UINib.init(nibName: "FeatureCellCompact", bundle: nil), forCellWithReuseIdentifier: "FEATURECellCompact")
        self.collectionView?.registerNib(UINib.init(nibName: "FeatureCellCompact", bundle: nil), forCellWithReuseIdentifier: "FEATURECellNormal")

        let showFirstCell: Bool = (self.restorationIdentifier! == Constants.Menu.Home.viewName)
        let collectionConfig = Constants.PageCollectionSettings[self.title!]
        self.Delegate = FirstCellCollectionViewDelegate.init(collectionView: self.collectionView!, collectionConfig: collectionConfig!, articles: self.articles, showFirstCell: showFirstCell, pageIdentifier: self.restorationIdentifier!, inFirstCell: false, selectionBlock: {(article:Article) in
            self.displayDetailViewForArticleWithKey(article.key)
            },
            scrollBlock: { (offset: CGFloat, scrollingDown: Bool) in
                self.animateBanner(offset, down: scrollingDown)
                if self.refreshPanel != nil {
                    self.updateRefreshPanel(offset, down: scrollingDown)
                }
            },
            playBlock: {(url:String) in
                Utilities.displayUrlInWebViewController("http:" + url, presentingViewController: self)
            },
            imdbBlock: {(key: String) in
                Utilities.displayUrlInWebViewController("http://imdb.com/title/" + key, presentingViewController: self)
            },
            favBlock: {(key: String) in
                Utilities.toggleFavourite(key)
            }
        )
        
        self.fetchedResultController = self.articleResultController()
        self.collectionView!.delegate = self.Delegate
        self.collectionView!.dataSource = self.Delegate
        
        self.refreshCollectionBackgroundView((self.restorationIdentifier! == Constants.Menu.Favourites.viewName && self.articles.count == 0))

        if self.restorationIdentifier! == Constants.Menu.Home.viewName {
            self.addRefreshBar()
        }
        
        NSNotificationCenter.defaultCenter().addObserverForName(Constants.Notifications.favouritesChanged, object: nil, queue: nil, usingBlock: { (NSNotification) in
            self.refreshCollectionBackgroundView((self.restorationIdentifier! == Constants.Menu.Favourites.viewName && self.articles.count == 0))
        })
    }
    
    func refreshCollectionBackgroundView(showNoFavourite: Bool) {
        if (showNoFavourite) {
            let label = UILabel()
            label.text = "You have not added any favourites..."
            label.textColor = UIColor.lightGrayColor()
            label.textAlignment = .Center
            label.font = UIFont(name: label.font.fontName, size: 30)
            label.numberOfLines = 5
            label.alpha = 0.4
            self.collectionView?.backgroundView = label
        } else {
            let bgView = UIImageView(image: UIImage(named: "logo"))
            Utilities.blurView(bgView)
            self.collectionView?.backgroundView = bgView
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func traitCollectionDidChange(previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        if self.collectionView!.indexPathsForVisibleItems().count > 0 {
            self.collectionView!.reloadItemsAtIndexPaths(self.collectionView!.indexPathsForVisibleItems())
        }
        if let refreshPanel = self.refreshPanel {
            var frame = refreshPanel.frame
            frame.size.width = (self.collectionView?.frame.size.width)!
            refreshPanel.frame = frame
        }
    }
    
    override func viewWillTransitionToSize(size: CGSize, withTransitionCoordinator coordinator: UIViewControllerTransitionCoordinator) {
        self.collectionView!.collectionViewLayout.invalidateLayout()
    }
    
    //TODO: Maybe these methods belong in the model ?
    func articleResultController() -> NSFetchedResultsController {
        let appDelegate: AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        let moc: NSManagedObjectContext = appDelegate.coreDataStack.mainManagedObjectContext
        fetchedResultController = NSFetchedResultsController(fetchRequest: articlesFetchRequest(),managedObjectContext: moc,sectionNameKeyPath: nil,cacheName: nil)
        fetchedResultController.delegate = self
        do {
            try fetchedResultController.performFetch()
            var articles: [Article] = []
            for object in fetchedResultController.fetchedObjects! {
                let article: Article = object as! Article
                articles.append(article)
            }
            self.articles = articles
            self.Delegate.articles = self.articles
            self.collectionView!.reloadData()
        } catch let error as NSError {
            print("Error: \(error.localizedDescription)")
            abort()
        }
        return fetchedResultController
    }
    
    func articlesFetchRequest() -> NSFetchRequest {
        let fetchRequest = NSFetchRequest(entityName: "Article")
        fetchRequest.fetchBatchSize = 100
        let sortDescriptor = NSSortDescriptor(key: "published", ascending: false)
        fetchRequest.sortDescriptors = [sortDescriptor]
        if let typeFilter = self.identifierToTypeMap[self.restorationIdentifier!] {
            fetchRequest.predicate = NSPredicate(format: String(typeFilter))
        }
        return fetchRequest
    }
    
    func controllerDidChangeContent(controller: NSFetchedResultsController) {
        self.articleResultController()
    }
    
    func displayDetailViewForArticleWithKey(key: String) {
        let newViewController = DetailViewController.init(articleKey: key, nibName: "DetailViewController", bundle: nil)
        self.presentViewController(newViewController!, animated: true, completion: nil)
    }
    
    func animateBanner(offsetWhenScrollStopped:CGFloat, down: Bool) {
        let rootViewController: RootViewController = self.parentViewController as! RootViewController
        let currentBannerHeight: CGFloat = rootViewController.bannerHeight.constant
        var offsetSinceLastStopped = abs((self.collectionView?.contentOffset.y)! - offsetWhenScrollStopped)
        offsetSinceLastStopped = pow(offsetSinceLastStopped / 100,2)
        var bannerHeight: CGFloat
        if down == false {
            offsetSinceLastStopped = offsetSinceLastStopped * -1
        }
        bannerHeight = min(Constants.bannerHeight, max(Constants.statusBarHeight, currentBannerHeight + offsetSinceLastStopped))
        let bannerAlpha = pow(bannerHeight,4) / pow(Constants.bannerHeight,4)
        rootViewController.bannerHeight.constant = bannerHeight
        rootViewController.appTitleLabel.alpha = bannerAlpha
        rootViewController.searchButton.alpha = bannerAlpha
        rootViewController.searchTextButton.alpha = bannerAlpha
    }
    
    func addRefreshBar() {
        let backgroundView = self.collectionView?.backgroundView

        self.refreshPanel = UILabel(frame:CGRectMake(0,70,(self.collectionView?.frame.size.width)!, Constants.refreshPanelHeight))
        self.refreshPanel.text = "Keep pulling to refresh..."
        self.refreshPanel.textColor = UIColor.whiteColor()
        self.refreshPanel.textAlignment = .Center
        self.refreshPanel.font = UIFont(name: self.refreshPanel.font.fontName, size: 25)
        self.refreshPanel.numberOfLines = 1
        self.refreshPanel.alpha = 0.0
        self.refreshPanel.backgroundColor = Constants.Colors.mainTheme
        backgroundView?.addSubview(refreshPanel)
        backgroundView?.bringSubviewToFront(refreshPanel)
    }

    func updateRefreshPanel(offset: CGFloat, down: Bool) {
        if let offsetY = self.collectionView?.contentOffset.y {
            self.refreshPanel.alpha = (offsetY * -1) / Constants.refreshPanelHeight
            if offsetY < 0 {
                if offsetY < -Constants.refreshPanelHeight {
                    self.collectionView?.contentOffset.y = -Constants.refreshPanelHeight
                    if !self.refreshingArticles {
                        self.refreshingArticles = true
                        self.refreshPanel.text = "Refresh in progress..."
                        ModelController().refreshArticles() {
                            Utilities.runOnMainQueueAfterDelayInSeconds(0.0) {
                                self.refreshPanel.text = "Refresh complete"
                            }
                        }
                        
                        //start a timer to re-allow refresh
                        Utilities.runOnMainQueueAfterDelayInSeconds(120.0) {
                            self.refreshingArticles = false
                            self.refreshPanel.text = "Keep pulling to refresh..."
                        }
                    }
                }
            }
        }
    }

}


