//
//  Created by Patrick Hogan on 16/11/2015.
//  Copyright © 2015 Patrick Hogan. All rights reserved.
//

import Foundation
import UIKit

class FirstCellCollectionViewDelegate: NSObject, UICollectionViewDataSource, UICollectionViewDelegate {
    
    var downloadManager: ImageDownloadManager!
    var collectionView: UICollectionView!
    var articles: [Article] = []
    var selectionBlock: ((Article) -> Void)! = nil
    var showFirstCell: Bool! = false
    var inFirstCell: Bool! = false
    var pageIdentifier: String!
    var scrollBlock: ((offset: CGFloat, scrollingDown: Bool) -> Void)! = nil

    //TODO: Build a collection of action handlers rather than passing them individually
    var playBlock: ((url: String) -> Void)? = nil
    var imdbBlock: ((key: String) -> Void)? = nil
    var favBlock: ((key: String) -> Void)? = nil
    var collectionConfig: CollectionConfig!
    
    var scrollOffsetY: CGFloat = 0
    var lastContentOffset: CGFloat = 0
    var scrollingDown: Bool = false
    var previouslyScrollingDown: Bool = false
    
    init?(collectionView: UICollectionView, collectionConfig: CollectionConfig, articles: [Article], showFirstCell: Bool, pageIdentifier: String?, inFirstCell: Bool, selectionBlock: ((Article) -> Void), scrollBlock: ((offset: CGFloat, scrollingDown: Bool) -> Void)?, playBlock: ((url: String) -> Void)?, imdbBlock: ((key: String) -> Void)?, favBlock: ((key: String) -> Void)?) {
        super.init()
        self.showFirstCell = showFirstCell
        self.inFirstCell = inFirstCell
        self.collectionView = collectionView
        self.articles = articles
        self.pageIdentifier = pageIdentifier
        self.selectionBlock = selectionBlock
        self.scrollBlock = scrollBlock
        self.playBlock = playBlock
        self.imdbBlock = imdbBlock
        self.favBlock = favBlock
        self.collectionConfig = collectionConfig 
        downloadManager = ImageDownloadManager(completionBlock: { (imageDownload: ImageDownload) in
            self.processDownloadImage(imageDownload)
        })!
    }
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        let rowCount = showFirstCell == false ? self.articles.count : self.articles.count > 0 ? max(self.articles.count - Constants.numArticlesInFirstCell, 0) + 1 : 0
        return rowCount
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        if self.showFirstCell == true && indexPath.item == 0 {
            let cellIdentifier = cellIdentifierFromIndexPath(indexPath)
            let cell: FirstCollectionViewCell = collectionView.dequeueReusableCellWithReuseIdentifier(cellIdentifier, forIndexPath: indexPath) as! FirstCollectionViewCell
            let numArticles = min(Constants.numArticlesInFirstCell,self.articles.count)
            let array:[Article] = Array(self.articles[0..<numArticles])
            cell.configureWithArticles(array, selectionBlock: self.selectionBlock, playBlock: self.playBlock, imdbBlock: self.imdbBlock, favBlock: self.favBlock)
            return cell
            
        } else {
            let cellIdentifier = cellIdentifierFromIndexPath(indexPath)
            let cell: CollectionViewCell = collectionView.dequeueReusableCellWithReuseIdentifier(cellIdentifier, forIndexPath: indexPath) as! CollectionViewCell
            let article: Article = self.articleFromIndexPath(indexPath)
            cell.configureCellWithArticle(article, playActionHandler: {
                if let trailerUrl = article.trailerUrl {
                    if let playBlock = self.playBlock {
                        playBlock(url: trailerUrl)
                    }
                }
                }, imdbActionHandler: {
                    if let imdbKey = article.imdbKey {
                        if let imdbBlock = self.imdbBlock {
                            imdbBlock(key: imdbKey)
                        }
                    }
                }, favActionHandler: {
                    if let favBlock = self.favBlock {
                        favBlock(key: article.key)
                    }
                }, deleteActionHandler: {
                    ModelController().removeArticleFavourite(article.key)
                }
            )
            if article.image == nil {
                self.downloadManager?.addUrl(NSURL(string:article.imageUrl()!)!, key: String(indexPath.item))
            }
            return cell
        }
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        
        var cellHeight: CGFloat = 0
        var cellWidth: CGFloat = 0
        
        //Setup a few constants that we will need to calculate cell dimensions
        let flowLayout = self.collectionView.collectionViewLayout as! UICollectionViewFlowLayout
        let collectionSize = (self.collectionView?.frame.size)!
        let widthMultiplier = Utilities.compactWidth() ? self.collectionConfig.widthMultiplierCompact : self.collectionConfig.widthMultiplierNormal
        let heightMultiplier = Utilities.compactWidth() ? self.collectionConfig.heightMultiplierCompact : self.collectionConfig.heightMultiplierNormal

        //Note: This should really be calculated based on scrolling direction
        let spacing = flowLayout.minimumInteritemSpacing
        
        //FirstCell
        if self.showFirstCell == true && indexPath.item == 0 {
            cellWidth = collectionSize.width
            if Utilities.landscape() {
                cellHeight = Utilities.compactWidth() ? Constants.CellSize.FirstCell.compactHeight : Constants.CellSize.FirstCell.normalHeight
            } else {
                cellHeight = Utilities.compactWidth() ? Constants.CellSize.FirstCell.compactHeight : Constants.CellSize.FirstCell.normalHeight
            }
        } else {
            
            //We allow cell width to be provided as a multiplier of collection width
            if widthMultiplier > 0 {
                cellWidth = ((collectionSize.width - ((CGFloat(widthMultiplier) + 1) * spacing)) / CGFloat(widthMultiplier))
            } else {
                if Utilities.landscape() {
                    cellWidth = Utilities.compactWidth() ? self.collectionConfig.widthCompactLandscape : self.collectionConfig.widthNormalLandscape
                } else {
                    cellWidth = Utilities.compactWidth() ? self.collectionConfig.widthCompact : self.collectionConfig.widthNormal
                }
            }

            //At the moment we use the horizontal size class to determine config
            if heightMultiplier > 0 {
                cellHeight = ((collectionSize.height - ((CGFloat(heightMultiplier) + 1) * spacing)) / CGFloat(heightMultiplier))
            } else {
                if Utilities.landscape() {
                    cellHeight = Utilities.compactWidth() ? self.collectionConfig.heightCompactLandscape : self.collectionConfig.heightNormalLandscape
                } else {
                    cellHeight = Utilities.compactWidth() ? self.collectionConfig.heightCompact : self.collectionConfig.heightNormal
                }
            }
        }
        return CGSizeMake(cellWidth,cellHeight)
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {

        if Utilities.landscape() {
            return Utilities.compactWidth() ? collectionConfig.sectionInsetLandscapeCompact : collectionConfig.sectionInsetLandscapeNormal
        } else {
            return Utilities.compactWidth() ? collectionConfig.sectionInsetPortraitCompact : collectionConfig.sectionInsetPortraitNormal
        }
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        let article: Article = self.articleFromIndexPath(indexPath)
        self.selectionBlock(article)
    }
    
    func processDownloadImage(imageDownload: ImageDownload) {
        
        if imageDownload.failed == false {
            let index: String = imageDownload.key!
            let indexPath: NSIndexPath = NSIndexPath(forItem: Int(index)!, inSection: 0)
            if let image = imageDownload.image {
                let article = self.articleFromIndexPath(indexPath)
                article.image = image
                if article.imageUrl() == imageDownload.url.absoluteString {
                    let collectionView = self.collectionView
                    if let cell = collectionView.cellForItemAtIndexPath(indexPath) {
                        let collectionCell: CollectionViewCell = cell as! CollectionViewCell
                        Utilities.runOnMainQueueAfterDelayInSeconds(0.0) {
                            collectionCell.image.image = image
                        }
                    } else {
                        dispatch_after(dispatch_time(DISPATCH_TIME_NOW,Int64(0.25)), dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), {
                            self.processDownloadImage(imageDownload)
                        })
                    }
                    
                }
            }
        }
    }
    
    func articleFromIndexPath(indexPath: NSIndexPath) -> Article {
        if showFirstCell == false {
            return self.articles[indexPath.item]
        } else {
            let index = min(indexPath.item + Constants.numArticlesInFirstCell - 1,self.articles.count - 1)
            return self.articles[index]
        }
    }
    
    func cellIdentifierFromIndexPath(indexPath: NSIndexPath) -> String {
        if showFirstCell == true && indexPath.item == 0 {
            return "FirstCell"
        } else if showFirstCell == true {
            if Utilities.compactWidth() {
                return "LatestCellCompact"
            } else {
                return "LatestCellNormal"
            }
        } else if inFirstCell == true {
            return "FirstCellDetail"
        } else if self.pageIdentifier == Constants.Menu.Favourites.viewName {
            return Utilities.compactWidth() ? "FavouriteCellCompact" : "FavouriteCellNormal"
        } else {
            return self.cellIdentifierForArticle(articleFromIndexPath(indexPath))
        }
    }
    
    func cellIdentifierForArticle (article: Article) -> String {
        var cellIdentifierPrefix = article.type
        
        //TODO: Provide a mapping between type and cell identifier
        if cellIdentifierPrefix == "SONG" { cellIdentifierPrefix = "FEATURE" }
        if cellIdentifierPrefix == "SONGYEAR" { cellIdentifierPrefix = "FEATURE" }
        if cellIdentifierPrefix == "STUFF" { cellIdentifierPrefix = "NEWS" }
        
        let cellIdentifierSuffix = Utilities.compactWidth() ? "Compact" : "Normal"
        
        return cellIdentifierPrefix + "Cell" + cellIdentifierSuffix
    }
    
    func scrollViewDidScroll(scrollView: UIScrollView) {
        
        NSNotificationCenter.defaultCenter().postNotificationName(Constants.Notifications.deleteCencelled, object: nil)

        if self.scrollBlock != nil {
            if self.lastContentOffset > scrollView.contentOffset.y {
                self.scrollingDown = true
            }
            if self.previouslyScrollingDown != self.scrollingDown {
                self.scrollOffsetY = scrollView.contentOffset.y
            }
            self.previouslyScrollingDown = self.scrollingDown
            self.lastContentOffset = scrollView.contentOffset.y
            self.scrollBlock(offset: self.scrollOffsetY, scrollingDown: self.scrollingDown)
        }
    }
    
    func scrollViewWillBeginDragging(scrollView: UIScrollView) {
        let translation = scrollView.panGestureRecognizer.translationInView(self.collectionView)
        self.scrollingDown = translation.x > 0
    }
    
}

