//
//  TheLastWordOnEarth
//
//  Created by Patrick Hogan on 28/10/2015.
//  Copyright © 2015 Patrick Hogan. All rights reserved.
//

import UIKit
import CoreData
import Crashlytics

class SearchViewController: UIViewController, UITableViewDelegate ,UITableViewDataSource, UISearchBarDelegate {

    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    var resultSearchController = UISearchController()
    var searchResults = [SearchResult]()
    var downloadManager: ImageDownloadManager!
    var latestSearchText: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        downloadManager = ImageDownloadManager(completionBlock: { (imageDownload: ImageDownload) in
            self.processDownloadImage(imageDownload)
        })!
        self.tableView?.registerNib(UINib.init(nibName: "SearchResultCellView", bundle: nil), forCellReuseIdentifier: "searchResultCell")
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.backgroundColor = UIColor.clearColor()
        self.searchBar.delegate = self
        self.searchBar.tintColor = Constants.Colors.mainTheme
        self.searchBar.showsCancelButton = true
        self.searchBar.keyboardType = .ASCIICapable
        self.searchBar.keyboardAppearance = .Dark
        self.searchBar.autocorrectionType = .No
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.searchBar.becomeFirstResponder()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.searchResults.count
    }

    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if self.traitCollection.horizontalSizeClass == UIUserInterfaceSizeClass.Compact {
            return CGFloat(Constants.CellSize.SearchResult.compactHeight)
        } else {
            return CGFloat(Constants.CellSize.SearchResult.normalHeight)
        }
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 114.0
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let searchResult: SearchResult = self.searchResults[indexPath.row]
        let newViewController = DetailViewController.init(articleKey: searchResult.key, nibName: "DetailViewController", bundle: nil)
        self.presentViewController(newViewController!, animated: true, completion: nil)
    }

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell: SearchResultCell = tableView.dequeueReusableCellWithIdentifier("searchResultCell", forIndexPath: indexPath) as! SearchResultCell
        if (indexPath.row < self.searchResults.count ) {
            let searchResult: SearchResult = self.searchResults[indexPath.row]
            cell.postDate.text! = searchResult.postDateDisplay
            cell.headline.text! = searchResult.headline
            cell.film.text = searchResult.film.camelCasedString
            var image: UIImage? = nil
            if searchResult.smallImage != nil {
                image = searchResult.smallImage
                cell.smallImage.image = image
                cell.smallImage.alpha = 1.0;
            } else {
                self.downloadManager?.addUrl(NSURL(string:searchResult.smallImageUrl!)!, key: String(indexPath.row))
            }
            cell.showImageForType(searchResult.type)
        }
        return cell
    }
    
    func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
        if (searchText.characters.count > 1) {
            Utilities.showSpinnerInView(self.view)
            self.downloadManager!.cancelAllOperations()
            //self.searchResults = []
            Answers.logCustomEventWithName("Search Tapped", customAttributes: ["Search Term": searchText])
            self.latestSearchText = searchText
            ModelController().searchArticlesWithSearchTerm(searchText, processSearchResults: { (results: [SearchResult], forSearchTerm: String) -> Void in
                Utilities.runOnMainQueueAfterDelayInSeconds(0.0) {
                    if forSearchTerm == self.latestSearchText {
                        self.searchResults = results
                        self.tableView.reloadData()
                        Utilities.hideSpinnerInView(self.view)
                    }
                }
            })
        }
    }
    
    func searchBarCancelButtonClicked(searchBar: UISearchBar) {
        NSNotificationCenter.defaultCenter().postNotificationName(Constants.Notifications.searchCencelled, object: nil)
    }
        
    func processDownloadImage(imageDownload: ImageDownload) {
        
        //TODO: There is an index out of range bug in here if multiple searches
        if imageDownload.failed == false && imageDownload.cancelled == false {
            let index: String = imageDownload.key!
            var searchResult: SearchResult = self.searchResults[Int(index)!]
            if searchResult.smallImageUrl == imageDownload.url.absoluteString {
                searchResult.smallImage = imageDownload.image
                self.searchResults[Int(index)!] = searchResult
                let indexPath: NSIndexPath = NSIndexPath(forRow: Int(index)!, inSection: 0)
                var indexPaths: [NSIndexPath] = []
                indexPaths.append(indexPath)
                Utilities.runOnMainQueueAfterDelayInSeconds(0.0) {

                    //TODO: This line can crash -> Callbacks are coming back for indexPaths when table results are empty (i.e. should have been cancelled)
                    if imageDownload.failed == false && imageDownload.cancelled == false {
                        self.tableView.reloadRowsAtIndexPaths(indexPaths, withRowAnimation: UITableViewRowAnimation.None)
                    }
                }
            }
        }
    }
    
    func searchBarSearchButtonClicked(searchBar: UISearchBar) {
        self.searchBar.endEditing(true)
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.searchBar.endEditing(true)
    }
    
}

