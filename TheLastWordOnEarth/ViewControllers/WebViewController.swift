//
//  Created by Patrick Hogan on 19/11/2015.
//  Copyright © 2015 Patrick Hogan. All rights reserved.
//

import UIKit
import WebKit
import SafariServices

class WebViewController: UIViewController, SFSafariViewControllerDelegate, WKNavigationDelegate {

    @IBOutlet weak var urlTextField: UITextField!
    @IBOutlet weak var doneButton: UIButton!
    @IBOutlet weak var webContainerView: UIView!
    
    @IBAction func doneButtonTapped(sender: AnyObject) {
        UIApplication.sharedApplication().statusBarStyle = .LightContent
        self.dismissViewControllerAnimated(false, completion: nil)
    }
    
    var presentingVC: UIViewController?
    var urlString: String = ""
    
    convenience init?(urlString: String, presentingViewController: UIViewController) {
        self.init(nibName: "WebViewController", bundle: nil)
        self.presentingVC = presentingViewController
        self.urlString = urlString
        
    }
    
    func display() {
        if #available(iOS 9.0, *) {
            self.displaySafariViewController()
        } else {
            self.displayWebView()
        }
    }
    
    @available(iOS 9.0, *)
    func displaySafariViewController() {
        UIApplication.sharedApplication().statusBarStyle = .Default
        let vc = SFSafariViewController(URL: NSURL(string: self.urlString)!)
        vc.view.tintColor = Constants.Colors.mainTheme
        self.presentingVC!.presentViewController(vc, animated: true, completion: nil)
    }

    @available(iOS 9.0, *)
    func safariViewControllerDidFinish(controller: SFSafariViewController) {
        UIApplication.sharedApplication().statusBarStyle = .LightContent
    }
    
    func displayWebView() {
        UIApplication.sharedApplication().statusBarStyle = .Default
        self.presentingVC!.presentViewController(self,animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        self.urlTextField.text = self.urlString
        self.doneButton.tintColor = Constants.Colors.mainTheme
        self.initializeWebView()
    }
    
    func initializeWebView() {
        
        //Create size notification handler
        let sizeNotificationJavascript = "window.webkit.messageHandlers.sizeNotification.postMessage({width: document.width, height: document.height});"
        let script: WKUserScript = WKUserScript.init(source: sizeNotificationJavascript, injectionTime:.AtDocumentEnd, forMainFrameOnly:true)
        let controller: WKUserContentController = WKUserContentController()
        controller.addUserScript(script)
        
        //Create a WKWebView and add it to the web container
        let webConfiguration: WKWebViewConfiguration = WKWebViewConfiguration()
        webConfiguration.userContentController = controller
        //webConfiguration.preferences.minimumFontSize = 17.0
        let webView = WKWebView(frame: self.webContainerView.bounds, configuration:webConfiguration)
        webView.navigationDelegate = self
        webView.loadRequest(NSURLRequest(URL: NSURL(string: self.urlString)!))
        webContainerView.addSubview(webView)
        webContainerView.bringSubviewToFront(webView)
        webView.translatesAutoresizingMaskIntoConstraints = false
        var constraints = [NSLayoutConstraint]()
        constraints.append(NSLayoutConstraint(item: webView,attribute: .Top, relatedBy: .Equal, toItem: self.webContainerView,attribute: .Top, multiplier: 1, constant: 0))
        constraints.append(NSLayoutConstraint(item: webView,attribute: .Height, relatedBy: .Equal, toItem: self.webContainerView,attribute: .Height, multiplier: 1, constant: 0))
        constraints.append(NSLayoutConstraint(item: webView,attribute: .Width, relatedBy: .Equal, toItem: self.webContainerView,attribute: .Width, multiplier: 1, constant: 0))
        constraints.append(NSLayoutConstraint(item: webView,attribute: .Bottom, relatedBy: .Equal, toItem: self.webContainerView,attribute: .Bottom, multiplier: 1, constant: 0))
        self.webContainerView.addConstraints(constraints)
    }

}
