//
//  TheLastWordOnEarth
//
//  Created by Patrick Hogan on 30/10/2015.
//  Copyright © 2015 Patrick Hogan. All rights reserved.
//

import UIKit
import WebKit
import SafariServices
import Crashlytics

class DetailViewController: UIViewController, WKNavigationDelegate, UIScrollViewDelegate, WKScriptMessageHandler {
    
    @IBOutlet weak var lblHeadline: UILabel!
    @IBOutlet weak var smallImageView: UIImageView!
    @IBOutlet weak var webContainerView: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var contentViewHeight: NSLayoutConstraint!
    @IBOutlet weak var imdbButton: UIButton!
    @IBOutlet weak var faveButton: UIButton!
    @IBOutlet weak var sidebarView: UIView!
    @IBOutlet weak var verticalMarkerTop: NSLayoutConstraint!
    @IBOutlet weak var sideBarTrailing: NSLayoutConstraint!
    
    @IBAction func faveButtonTapped(sender: AnyObject) {
        if let article = self.article {
            Utilities.toggleFavourite(article.key)
        }
    }
    
    @IBAction func imdbButtonTapped(sender: AnyObject) {
        self.displayIMDbPage()
    }
    
    @IBAction func shareButtonTapped(sender: AnyObject) {
        self.displayShareOptions()
    }

    @IBAction func btnDoneTapped(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
        UIApplication.sharedApplication().statusBarStyle = .Default
        Utilities.hideSpinnerInView(UIApplication.sharedApplication().windows.first!)
    }

    @IBAction func playButtonTapped(sender: AnyObject) {
        if let article = self.article {
            Utilities.displayUrlInWebViewController("http:" + article.trailerUrl!, presentingViewController: self)
        }
    }
    
    var article: Article?
    var downloadManager: ImageDownloadManager!
    var articleKey: String?
    var initialImageHeight: CGFloat = 0
    var initialWebContentHeight: CGFloat = 0
    
    convenience init?(articleKey:String, nibName: String, bundle: NSBundle?) {
        self.init(nibName: nibName, bundle: bundle)
        self.articleKey = articleKey
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        UIApplication.sharedApplication().statusBarStyle = .LightContent
        Utilities.showSpinnerInView(UIApplication.sharedApplication().windows.first!)
        self.initialImageHeight = self.smallImageView.frame.size.height
        self.initialWebContentHeight = self.webContainerView.frame.size.height
        self.downloadManager = ImageDownloadManager(completionBlock: { (imageDownload: ImageDownload) in
            Utilities.runOnMainQueueAfterDelayInSeconds(0) {
                self.processDownloadImage(imageDownload)
            }
        })!
        self.configureSidebar()
        self.fetchArticleWithKey(self.articleKey!)
        self.scrollView.delegate = self
        
        if let mainWindow = UIApplication.sharedApplication().windows.first {
            self.verticalMarkerTop.constant = mainWindow.frame.height / 2
        }
        
    }
    
    func fetchArticleWithKey(key: String) {
        ModelController().articleWithKey(key, processArticle: { (articleID: AnyObject) -> Void in
            self.setupWithArticle(articleID)
        })
    }
    
    func setupWithArticle(articleID: AnyObject) {
        ModelController().articleWithObjectID(articleID, processArticle: { (article: Article) -> Void in
            self.downloadManager.addUrl(NSURL(string:article.imageUrl()!)!, key: "DUMMY")
            
            Answers.logCustomEventWithName("Story Viewed", customAttributes: ["Headline": article.headline])
            
            Utilities.runOnMainQueueAfterDelayInSeconds(0) {
                self.lblHeadline.text = article.headline
                self.displayStory(article)
                self.article = article
                if article.imdbKey == nil || article.imdbKey == "" {
                    self.imdbButton.alpha = 0.35
                    self.imdbButton.enabled = false
                }
                Utilities.hideSpinnerInView(UIApplication.sharedApplication().windows.first!)
            }
        })
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func scrollViewDidScroll(scrollView: UIScrollView) {
        self.smallImageView.alpha = self.smallImageView.frame.size.height / self.initialImageHeight
        self.sideBarTrailing.constant = -1 * max(0,((scrollView.contentOffset.y - 40) / 3))
    }
    
    func processDownloadImage(imageDownload: ImageDownload) {
        if imageDownload.failed == false {
            Utilities.runOnMainQueueAfterDelayInSeconds(0.0) {
                self.smallImageView.alpha = 0
                self.smallImageView.image = imageDownload.image
                UIView.animateWithDuration(
                    1.0,
                    delay: 0,
                    options: .CurveEaseIn,
                    animations: {
                        self.smallImageView.alpha = 1.0
                    },
                    completion: nil
                )
            }
        }
    }
    
    func displayStory(article: Article) {

        if let story = article.story {
            
            let head: String = "<html><HEAD><meta name='viewport' content='initial-scale=1.0' /></HEAD>"
            let bodyStart: String = "<body><style>{max-width:100%%;height:auto !important; width:auto !important;};</style><div id='main-wrapper' style='word-wrap: break-word;'>"
            let bodyEnd: String = "</div></body></html>"
            let html: String = head + bodyStart + self.removeImages(story) + bodyEnd
            self.initializeWebViewWithHTML(html)
        }
    }
    
    func displayShareOptions() {

        if let article = self.article {
            let branchUniversalObject: BranchUniversalObject = BranchUniversalObject(title: "The Last Word On Earth")
            branchUniversalObject.contentDescription = article.headline
            branchUniversalObject.imageUrl = article.imageUrl()
            branchUniversalObject.addMetadataKey("key", value: article.key)
            let linkProperties: BranchLinkProperties = BranchLinkProperties()
            linkProperties.feature = "sharing"
            linkProperties.channel = "facebook"
            branchUniversalObject.getShortUrlWithLinkProperties(linkProperties,  andCallback: { (url: String?, error: NSError?) -> Void in
                if error == nil {
                    NSLog("Got my Branch link to share: %@", url!)
                }
            })
            branchUniversalObject.showShareSheetWithLinkProperties(linkProperties,
                andShareText: "Hey look at this...",
                fromViewController: self,
                andCallback: { () -> Void in
                    branchUniversalObject.registerView()
                    NSLog("done showing share sheet!")
            })
        }
    }
    
    
    func displayIMDbPage() {
        if let imdbKey = self.article!.imdbKey {
            if imdbKey != "" {
                Utilities.displayUrlInWebViewController("http://imdb.com/title/" + imdbKey, presentingViewController: self)
            }
        }
    }
    
    func configureSidebar() {
        self.sidebarView.backgroundColor = UIColor.whiteColor()
        self.sidebarView.alpha = 0.4
        
    }
    
    func removeImages(html: String) -> String {
        
        var newHtml = html
        if self.traitCollection.horizontalSizeClass == UIUserInterfaceSizeClass.Compact {
            if let rangeOfStartString = newHtml.lowercaseString.rangeOfString("<img") {
                let imageString = newHtml.substringFromIndex(rangeOfStartString.indices.first!)
                if let rangeOfEndString = imageString.lowercaseString.rangeOfString(">") {
                    let replaceString = imageString.substringToIndex(rangeOfEndString.indices.first!.successor())
                    newHtml = newHtml.stringByReplacingOccurrencesOfString(replaceString, withString: "")
                    newHtml = self.removeImages(newHtml)
                }
            }
        }
        return newHtml
    }
    
    func initializeWebViewWithHTML(html: String) {

        //Create size notification handler
        let sizeNotificationJavascript = "window.webkit.messageHandlers.sizeNotification.postMessage({width: document.width, height: document.height});"
        let script: WKUserScript = WKUserScript.init(source: sizeNotificationJavascript, injectionTime:.AtDocumentEnd, forMainFrameOnly:true)
        let controller: WKUserContentController = WKUserContentController()
        controller.addUserScript(script)
        controller.addScriptMessageHandler(self, name: "sizeNotification")
        
        //Create a WKWebView and add it to the web container
        let webConfiguration: WKWebViewConfiguration = WKWebViewConfiguration()
        webConfiguration.userContentController = controller
        webConfiguration.preferences.minimumFontSize = Utilities.compactWidth() ? 17.0 : 24.0
        let webView = WKWebView(frame: self.webContainerView.bounds, configuration:webConfiguration)
        webView.navigationDelegate = self
        webView.loadHTMLString(html, baseURL: nil)
        webContainerView.addSubview(webView)
        webContainerView.bringSubviewToFront(webView)
        webView.translatesAutoresizingMaskIntoConstraints = false
        var constraints = [NSLayoutConstraint]()
        constraints.append(NSLayoutConstraint(item: webView,attribute: .Top, relatedBy: .Equal, toItem: self.webContainerView,attribute: .Top, multiplier: 1, constant: 0))
        constraints.append(NSLayoutConstraint(item: webView,attribute: .Height, relatedBy: .Equal, toItem: self.webContainerView,attribute: .Height, multiplier: 1, constant: 0))
        constraints.append(NSLayoutConstraint(item: webView,attribute: .Width, relatedBy: .Equal, toItem: self.webContainerView,attribute: .Width, multiplier: 1, constant: 0))
        constraints.append(NSLayoutConstraint(item: webView,attribute: .Bottom, relatedBy: .Equal, toItem: self.webContainerView,attribute: .Bottom, multiplier: 1, constant: 0))
        self.webContainerView.addConstraints(constraints)
    }
    
    func userContentController(userContentController: WKUserContentController, didReceiveScriptMessage message: WKScriptMessage) {
        let height: CGFloat = message.body.valueForKey("height") as! CGFloat
        self.contentViewHeight.constant = self.webContainerView.frame.origin.y + max(height, self.initialWebContentHeight)
    }
}
