//
//  TheLastWordOnEarth
//
//  Created by Patrick Hogan on 28/10/2015.
//  Copyright © 2015 Patrick Hogan. All rights reserved.
//

import Foundation

extension NSDate {
    
    convenience
    init(dateString:String) {
        let dateStringFormatter = NSDateFormatter()
        dateStringFormatter.dateFormat = "YYYY MM dd HH:mm:ss"
        //dateStringFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX")
        let d = dateStringFormatter.dateFromString(dateString)!
        self.init(timeInterval:0, sinceDate:d)
    }

    func lastWordFormat() -> String {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "YYYY MMM dd HH:mm:ss"
        return dateFormatter.stringFromDate(self)
    }

    func lastWordFormatDisplay() -> String {
        let displayDateFormatter = NSDateFormatter()
        displayDateFormatter.dateFormat = "dd MMM YYYY"
        return displayDateFormatter.stringFromDate(self)
    }

}
