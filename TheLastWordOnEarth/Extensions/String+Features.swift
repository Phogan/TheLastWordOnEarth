//
//  TheLastWordOnEarth
//
//  Created by Patrick Hogan on 07/11/2015.
//  Copyright © 2015 Patrick Hogan. All rights reserved.
//

import Foundation

extension String {
    
    var camelCasedString: String {
        let source = self
        if source.characters.contains(" ") {
            let first = source.substringToIndex(source.startIndex.advancedBy(1))
            let cammel = NSString(format: "%@", (source as NSString).capitalizedString) as String
                
                
//                .stringByReplacingOccurrencesOfString(" ", withString: "", options: [], range: nil)) as String
            let rest = String(cammel.characters.dropFirst())
            return "\(first)\(rest)"
        } else {
            let first = (source as NSString).uppercaseString.substringToIndex(source.startIndex.advancedBy(1))
            let rest = String(source.characters.dropFirst()).lowercaseString
            return "\(first)\(rest)"
        }
    }
}