//
//  TheLastWordOnEarth
//
//  Created by Patrick Hogan on 21/10/2015.
//  Copyright © 2015 Patrick Hogan. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

enum Type: String {
    case News = "NEWS"
    case Review = "REVIEW"
    case Feature = "FEATURE"
    case Song = "SONG"
    case SongYear = "SONGYEAR"
    case Stuff = "STUFF"
}

extension Article {

    @NSManaged var headline: String
    @NSManaged var type: String
    @NSManaged var timestamp: NSDate
    @NSManaged var published: NSDate
    @NSManaged var key: String
    @NSManaged var imdbKey: String?
    @NSManaged var story: String?
    @NSManaged var subtitle: String?
    @NSManaged var smallImageUrl: String?
    @NSManaged var largeImageUrl: String?
    @NSManaged var favourite: Bool
    @NSManaged var trailerUrl: String?

}
