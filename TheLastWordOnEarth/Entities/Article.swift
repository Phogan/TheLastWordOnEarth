//
//  TheLastWordOnEarth
//
//  Created by Patrick Hogan on 21/10/2015.
//  Copyright © 2015 Patrick Hogan. All rights reserved.
//

import Foundation
import CoreData
import UIKit

class Article: NSManagedObject {

    var image: UIImage! = nil
    let displayDateFormatter = NSDateFormatter()
    
    func displayDate() -> String {
        self.displayDateFormatter.dateFormat = "dd MMM YYYY"
        return displayDateFormatter.stringFromDate(self.published)
    }
    
    func imageUrl() -> String? {
        return Utilities.compactWidth() ? self.smallImageUrl : (self.largeImageUrl != nil ? self.largeImageUrl : self.smallImageUrl)
    }

}
