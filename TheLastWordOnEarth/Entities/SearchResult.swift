//
//  SearchResult.swift
//  TheLastWordOnEarth
//
//  Created by Patrick Hogan on 29/10/2015.
//  Copyright © 2015 Patrick Hogan. All rights reserved.
//

import Foundation
import UIKit

struct SearchResult {

    let postDate: NSDate
    let film: String
    let headline: String
    let smallImageUrl: String?
    var smallImage: UIImage?
    let type: Type
    let key: String
    let postDateDisplay: String
    
}
