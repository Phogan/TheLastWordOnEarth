//
//  Created by Patrick Hogan on 19/11/2015.
//  Copyright © 2015 Patrick Hogan. All rights reserved.
//

import UIKit

class GradientViewCollectionViewCell: CollectionViewCell {

    @IBOutlet weak var gradientView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        if let gradient = self.gradientView {
            Utilities.addGradientToView(
                gradient,
                startColor: UIColor.clearColor().CGColor,
                endColor: UIColor.blackColor().colorWithAlphaComponent(1.0).CGColor,
                locations: [ 0.0, 1.0]
            )
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.layoutIfNeeded()
        if let gradient = self.gradientView {
            self.resizeGradientLayersInView(gradient)
        }
    }
//
//    override func traitCollectionDidChange(previousTraitCollection: UITraitCollection?) {
//        super.traitCollectionDidChange(previousTraitCollection)
//        if let gradient = self.gradientView {
//            self.resizeGradientLayersInView(gradient)
//        }
//    }
//
    func resizeGradientLayersInView(view: UIView) {
        for subLayer in view.layer.sublayers! {
            subLayer.frame = view.bounds
        }
    }

}