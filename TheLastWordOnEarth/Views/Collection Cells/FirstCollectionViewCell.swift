//
//  TheLastWordOnEarth
//
//  Created by Patrick Hogan on 10/11/2015.
//  Copyright © 2015 Patrick Hogan. All rights reserved.
//

import UIKit

class FirstCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var mainCollection: UICollectionView!
    @IBOutlet weak var arrowImageView: UIImageView!
    @IBOutlet weak var descriptionText1: UILabel!
    @IBOutlet weak var descriptionText2: UILabel!
    
    var arrow: UIImageView!
    
    override func awakeFromNib() {
        self.mainCollection?.registerNib(UINib.init(nibName: "FirstCellDetail", bundle: nil), forCellWithReuseIdentifier: "FirstCellDetail")
        self.configureBackgroundView()
        self.descriptionText1.textColor = Constants.Colors.mainTheme
        self.descriptionText2.textColor = Constants.Colors.mainTheme
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
    }
    
    func configureWithArticles(articles: [Article], selectionBlock: (Article) -> Void, playBlock: ((url: String) -> Void)?, imdbBlock: ((key: String) -> Void)?, favBlock: ((key: String) -> Void)? ) {
        let delegate = FirstCellCollectionViewDelegate.init(collectionView: self.mainCollection, collectionConfig: Constants.PageCollectionSettings[Constants.firstCellViewName]! , articles: articles, showFirstCell: false, pageIdentifier: nil, inFirstCell: true, selectionBlock: selectionBlock, scrollBlock: { (offset: CGFloat, scrollingDown: Bool) in
            UIView.animateWithDuration(1.5, animations: {
                if self.arrowImageView.alpha > 0 {
                    self.arrowImageView.alpha = 0
                }
            })
            },
            playBlock: playBlock,
            imdbBlock: imdbBlock,
            favBlock: favBlock
            
        )
        self.mainCollection.delegate = delegate
        self.mainCollection.dataSource = delegate
        self.mainCollection.reloadData()
    }
    
    func configureBackgroundView() {
        self.mainCollection.backgroundView = UIView()
        Utilities.addConstraintsForParentView(self.mainCollection, childView: self.mainCollection.backgroundView!)
    }
}
