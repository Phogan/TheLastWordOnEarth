//
//  TheLastWordOnEarth
//
//  Created by Patrick Hogan on 13/10/2015.
//  Copyright © 2015 Patrick Hogan. All rights reserved.
//

import UIKit

class CollectionViewCell: UICollectionViewCell {

    
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var headline: UILabel!
    @IBOutlet weak var subHeading: UILabel!
    @IBOutlet weak var posted: UILabel!
    @IBOutlet weak var playButton: UIButton!
    @IBOutlet weak var sidebar: UIView!

    @IBAction func playButtonTapped (sender: AnyObject) {
        if let playActionHandler = self.playActionHandler {
            playActionHandler()
        }
    }
    
    @IBAction func imdbButtonTapped (sender: AnyObject) {
        if let imdbActionHandler = self.imdbActionHandler {
            imdbActionHandler()
        }
    }

    @IBAction func favButtonTapped (sender: AnyObject) {
        if let favActionHandler = self.favActionHandler {
            favActionHandler()
        }
    }

    var playActionHandler: (() -> Void)? = nil
    var imdbActionHandler: (() -> Void)? = nil
    var favActionHandler: (() -> Void)? = nil
    var deleteActionHandler: (() -> Void)? = nil
    
    override func awakeFromNib() {
        super.awakeFromNib()
        if let posted = self.posted {
            posted.alpha = 0.7
            posted.textColor = Constants.Colors.mainTheme
        }
    }

    override func prepareForReuse() {
        self.playActionHandler = nil
        if let imageView = self.image {
            imageView.image = nil
        }
    }
    
    func configureCellWithArticle(article: Article, playActionHandler: (() -> Void)?, imdbActionHandler: (() -> Void)?, favActionHandler: (() -> Void)?, deleteActionHandler: (() -> Void)? = nil) {
        self.playActionHandler = playActionHandler
        self.imdbActionHandler = imdbActionHandler
        self.favActionHandler = favActionHandler
        self.deleteActionHandler = deleteActionHandler
        self.headline.text = article.headline
        
        if let playButton = self.playButton {
            if article.trailerUrl != nil && article.trailerUrl != "" {
                playButton.hidden = false
                if let sidebar = self.sidebar {
                    sidebar.hidden = false
                }
            } else {
                playButton.hidden = true
                if let sidebar = self.sidebar {
                    sidebar.hidden = true
                }
            }
        }
        
        if let posted = self.posted {
            posted.text = "Posted: " + article.displayDate()
        }
        if let image = article.image {
            UIView.animateWithDuration(0.35, animations: {
                self.image.image = image
            })
        }
        if let subtitle = article.subtitle {
            if let subHeading = self.subHeading {
                subHeading.text = subtitle
            }
        }
        
    }
}
