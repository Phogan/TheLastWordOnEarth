//
//  Created by Patrick Hogan on 31/10/2015.
//  Copyright © 2015 Patrick Hogan. All rights reserved.
//

import UIKit

class MenuCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var menuLabel: UILabel!
    @IBOutlet weak var menuImage: UIImageView!
    @IBOutlet weak var menuHighlight: UIView!
    @IBOutlet weak var overlayView: UIView!
    @IBOutlet weak var menuContentView: UIView!
    @IBOutlet weak var menuHighlightWidth: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        Utilities.addGradientToView(self.overlayView, startColor: UIColor.clearColor().CGColor, endColor: UIColor.blackColor().CGColor, locations: [0.0,1.0])
        Utilities.blurView(self,sendToBack: true)
    }
    
    override func prepareForReuse() {
    }
    
    func configureCell(cellSelected: Bool, traitCollection: UITraitCollection) {
        
        self.menuHighlight.backgroundColor = Constants.Colors.mainTheme
        if (traitCollection.horizontalSizeClass == UIUserInterfaceSizeClass.Compact) {
            self.menuImage.hidden = true
        } else if (traitCollection.horizontalSizeClass == UIUserInterfaceSizeClass.Regular) {
            self.menuImage.hidden = false
        } else  {
            
            //Default - hopefully this doesnt happen
            self.menuImage.hidden = true
        }
        
        if cellSelected {
            var barSize: CGFloat = self.menuHighlightWidth.constant
            if Utilities.compactWidth() {
                if let ns_str:NSString = self.menuLabel.text as NSString? {
                    let sizeOfString = ns_str.boundingRectWithSize(
                        CGSizeMake(self.menuLabel.frame.size.width, CGFloat.infinity),
                        options: NSStringDrawingOptions.UsesLineFragmentOrigin,
                        attributes: [NSFontAttributeName: self.menuLabel.font],
                        context: nil).size
                    barSize = sizeOfString.width
                }
            }
            self.menuHighlight.hidden = false
            self.menuHighlight.alpha = 0.0
            UIView.animateWithDuration(Constants.AnimationDuration.pageSwitch, delay: 0, options: .CurveEaseIn, animations: {
                self.menuHighlight.alpha = 1.0
                self.menuLabel.alpha = 1.0
                self.menuImage.alpha = 1.0
                self.menuHighlightWidth.constant = barSize
                }, completion: nil)
        } else {
            self.menuImage.alpha = 0.4
            self.menuLabel.alpha = 0.4
            self.menuHighlight.hidden = true
        }
    }
    
}
