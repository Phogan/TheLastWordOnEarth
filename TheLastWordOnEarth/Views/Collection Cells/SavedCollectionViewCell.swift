//
//  Created by Patrick Hogan on 16/12/2015.
//  Copyright © 2015 Patrick Hogan. All rights reserved.
//

import Foundation

class SavedCollectionViewCell: CollectionViewCell, UIGestureRecognizerDelegate {

    
    @IBOutlet weak var deleteButton: UIButton!
    
    @IBAction func deleteButtonTapped(sender: AnyObject) {
        NSNotificationCenter.defaultCenter().postNotificationName(Constants.Notifications.deleteCencelled, object: nil)
        if let deleteActionHandler = self.deleteActionHandler {
            deleteActionHandler()
        }
    }
    
    let thresholdWidth: CGFloat = -50
    let deleteButtonWidth: CGFloat = 100
    
    override func hitTest(point: CGPoint, withEvent event: UIEvent?) -> UIView? {
        if CGRectContainsPoint(self.deleteButton.frame, point) {
            return deleteButton
        } else {
            return super.hitTest(point, withEvent: event)
        }
    }
    
    override func prepareForReuse() {
        NSNotificationCenter.defaultCenter().removeObserver(self)
        super.prepareForReuse()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        //Add a pan gesture to allow deletion of saved articles
        self.clipsToBounds = false
        let panGesture: UIPanGestureRecognizer = UIPanGestureRecognizer(target: self, action: "respondToPan:")
        panGesture.delegate = self
        self.addGestureRecognizer(panGesture)
        
        let tapGesture: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "respondToTap:")
        tapGesture.delegate = self
        self.addGestureRecognizer(tapGesture)
        
        NSNotificationCenter.defaultCenter().addObserverForName(Constants.Notifications.deleteCencelled, object: nil, queue: nil, usingBlock: { (NSNotification) in
            self.hideDeleteButton()
        })
        
    }
    
    override func gestureRecognizerShouldBegin(gestureRecognizer: UIGestureRecognizer) -> Bool {
        if gestureRecognizer.isKindOfClass(UIPanGestureRecognizer) {
            let panGesture: UIPanGestureRecognizer = gestureRecognizer as! UIPanGestureRecognizer
            if panGesture.translationInView(self).y != 0  {
                return false
            } else {
                if panGesture.translationInView(self).x < 0 && self.frame.origin.x < 0 {
                    return false
                } else {
                    //TODO: If collection is scrolling we want to return false here
                    return true
                }
            }
        } else if gestureRecognizer.isKindOfClass(UITapGestureRecognizer) {
            if self.frame.origin.x < 0 {
                return true
            } else {
                NSNotificationCenter.defaultCenter().postNotificationName(Constants.Notifications.deleteCencelled, object: nil)
                return false
            }
        } else {
            return super.gestureRecognizerShouldBegin(gestureRecognizer)
        }
    }
    
    func gestureRecognizer(gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWithGestureRecognizer otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        if self.frame.origin.x < 0 {
            return false
        } else {
            return true
        }
    }
    
    func respondToPan(sender: UIPanGestureRecognizer) {
        
        switch sender.state {
        case .Began:
            NSNotificationCenter.defaultCenter().postNotificationName(Constants.Notifications.deleteCencelled, object: nil)
        case .Changed:
            var frame = self.frame
            let offsetX = sender.translationInView(self).x
            if offsetX < 0 && offsetX > -100  {
                frame.origin.x = offsetX
                self.frame = frame
            }
            self.deleteButton.alpha = 0.7 - (0.7 - (0.7 * abs(frame.origin.x / deleteButtonWidth)))
        case .Ended:
            var frame = self.frame
            let deleteButtonAlpha: CGFloat = 0.7
            if frame.origin.x >= -50 {
                frame.origin.x = 0
            } else {
                frame.origin.x = -100
            }
            UIView.animateWithDuration(0.25, animations: {
                self.deleteButton.alpha = deleteButtonAlpha
                self.frame = frame
            })
        default: break
        }
    }
    
    func respondToTap(sender: UIPanGestureRecognizer) {
        if self.frame.origin.x < 0 {
            self.hideDeleteButton()
        }
    }
    
    func hideDeleteButton() {
        var frame = self.frame
        frame.origin.x = 0
        UIView.animateWithDuration(0.35, animations: {
            self.frame = frame
        })
    }
}
