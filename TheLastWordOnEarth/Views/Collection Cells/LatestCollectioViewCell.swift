//
//  TheLastWordOnEarth
//
//  Created by Patrick Hogan on 19/11/2015.
//  Copyright © 2015 Patrick Hogan. All rights reserved.
//

import UIKit

class LatestCollectionViewCell: CollectionViewCell {
    
    @IBOutlet weak var headlineBackgroundView: UIView!
    @IBOutlet weak var imageBackground: UIView!
    
    override func prepareForReuse() {
        super.prepareForReuse()

        //TODO: Check to see what impact this has on performance
        self.resizeGradientLayersInView(self.headlineBackgroundView)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        Utilities.addGradientToView(
            self.headlineBackgroundView,
            startColor: UIColor.blackColor().CGColor,
            endColor: UIColor.clearColor().CGColor,
            locations: [0.0, 1.0]
        )
        self.imageBackground.clipsToBounds = true
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.imageBackground.layer.cornerRadius = self.imageBackground.frame.size.height / 2
    }
        
        
    override func traitCollectionDidChange(previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        self.resizeGradientLayersInView(self.headlineBackgroundView)
    }

    func resizeGradientLayersInView(view: UIView) {
        for subLayer in view.layer.sublayers! {
            subLayer.frame = view.bounds
        }
    }
    
}