//
//  TheLastWordOnEarth
//
//  Created by Patrick Hogan on 29/10/2015.
//  Copyright © 2015 Patrick Hogan. All rights reserved.
//

import UIKit

class SearchResultCell: UITableViewCell {

    @IBOutlet weak var smallImage: UIImageView!
    @IBOutlet weak var headline: UILabel!
    @IBOutlet weak var postDate: UILabel!
    @IBOutlet weak var film: UILabel!
    @IBOutlet weak var backgroundPanel: UIView!

    @IBOutlet weak var imageViewLatest: UIImageView!
    @IBOutlet weak var imageViewReview: UIImageView!
    @IBOutlet weak var imageViewFeature: UIImageView!
    @IBOutlet weak var imageViewOther: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()

        //TODO: We should be able to calculate the radius properly once the views have been layed out
        if Utilities.compactWidth() {
            smallImage.layer.cornerRadius = self.frame.size.height / 4
            smallImage.layer.borderWidth = 1.0
        } else {
            smallImage.layer.cornerRadius = 67
            smallImage.layer.borderWidth = 2.0
        }
        
        smallImage.layer.borderColor = UIColor.whiteColor().CGColor
        smallImage.clipsToBounds = true
        self.postDate.textColor = Constants.Colors.mainTheme
        self.backgroundColor = UIColor.clearColor()
        self.selectionStyle = UITableViewCellSelectionStyle.None
        
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.smallImage.image = nil
        self.imageViewLatest.hidden = true
        self.imageViewReview.hidden = true
        self.imageViewFeature.hidden = true
        self.imageViewOther.hidden = true
    }

    func showImageForType(type: Type) {
        switch type {
        case Type.News:
            self.imageViewLatest.hidden = false
        case Type.Review:
            self.imageViewReview.hidden = false
        case Type.Song:
            self.imageViewFeature.hidden = false
        default:
            self.imageViewOther.hidden = false
        }
    }


}
