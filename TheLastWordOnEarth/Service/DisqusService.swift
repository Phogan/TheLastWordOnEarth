//
//  Created by Patrick Hogan on 26/11/2015.
//  Copyright © 2015 Patrick Hogan. All rights reserved.
//

import Foundation

class DisqusService {

    let disqusApiKey = "sOwLhGNew74mzn4vjEnKLNf47jyrbkzwqjzt3O6xTiJJOZGh5wuQTZ03fI7oNCQS"
    
    func retrievePosts(processJson:(json: NSDictionary)->()) {
        ConnectionService.executeJSONRequestWithUrlString(
            Constants.Urls.disqusPosts,
            method: RequestMethod.get,
            params: [
                "forum" : "thelastword",
                "api_key" : self.disqusApiKey,
                "limit" : "100"
            ],
            successHandler: { (json: NSDictionary) in
                processJson(json: json)
            }
        )
    }
    
}
