//
//  Created by Patrick Hogan on 21/10/2015.
//  Copyright © 2015 Patrick Hogan. All rights reserved.
//

import Foundation

class LastWordService {
    
    let articleLimitGeneral = 100
    let articleLimitReviews = 15
    let articleLimitSong = 20
    let searchLimit = 20
    
    func retrieveArticlesSinceDate(timeStamp: NSDate, processJson:(json: NSDictionary)->()) {
        print("A:",timeStamp.lastWordFormat())
        ConnectionService.executeJSONRequestWithUrlString(
            Constants.Urls.latestArticles,
            method: RequestMethod.post,
            params: ["limit":"\(self.articleLimitGeneral)", "type":"", "since": "\(timeStamp.lastWordFormat())" ],
            successHandler: { (json: NSDictionary) in
                processJson(json: json)
            }
        )
        ConnectionService.executeJSONRequestWithUrlString(
            Constants.Urls.latestArticles,
            method: RequestMethod.post,
            params: ["limit":"\(self.articleLimitSong)", "type":"SONG", "since": "\(timeStamp.lastWordFormat())" ],
            successHandler: { (json: NSDictionary) in
                processJson(json: json)
            }
        )
        ConnectionService.executeJSONRequestWithUrlString(
            Constants.Urls.latestArticles,
            method: RequestMethod.post,
            params: ["limit":"\(self.articleLimitSong)", "type":"SONGYEAR", "since": "\(timeStamp.lastWordFormat())" ],
            successHandler: { (json: NSDictionary) in
                processJson(json: json)
            }
        )
        ConnectionService.executeJSONRequestWithUrlString(
            Constants.Urls.latestArticles,
            method: RequestMethod.post,
            params: ["limit":"\(self.articleLimitReviews)", "type":"REVIEW", "since": "\(timeStamp.lastWordFormat())" ],
            successHandler: { (json: NSDictionary) in
                processJson(json: json)
            }
        )
    }
    
    func retrieveArticlesMatchingSearchTerm(searchTerm: String, processJson:(json: NSDictionary)->()) {
        ConnectionService.executeJSONRequestWithUrlString(
            Constants.Urls.searchRequest,
            method: RequestMethod.post,
            params: ["limit":"\(self.searchLimit)", "searchString": "\(searchTerm)" ],
            successHandler: { (json: NSDictionary) in
                processJson(json: json)
            }
        )
    }
    
    func retrieveArticleWithKey(key: String, processJson:(json: NSDictionary)->()) {
        ConnectionService.executeJSONRequestWithUrlString(
            Constants.Urls.singleArticleRequest,
            method: RequestMethod.post,
            params: ["key":"\(key)"],
            successHandler: { (json: NSDictionary) in
                processJson(json: json)
            }
        )
    }
}