//
//  Created by Patrick Hogan on 26/11/2015.
//  Copyright © 2015 Patrick Hogan. All rights reserved.
//

import Foundation

enum RequestMethod: String {
    case post = "POST"
    case get = "GET"
}

class ConnectionService {
    
    class func executeJSONRequestWithUrlString(urlString: String, method: RequestMethod, params: Dictionary<String, String>, successHandler: (json: NSDictionary)->()) -> NSError? {
        
        var requestError: NSError? = nil
        
        //Create the request object
        let request: NSMutableURLRequest = NSMutableURLRequest(URL:NSURL(string: urlString)!)
        request.HTTPMethod = method.rawValue
        request.setValue("application/json", forHTTPHeaderField:"content-type")
        
        if method == RequestMethod.post {
            addPostDataToRequest(request, postData: params)
        } else {
            addQueryParamsToRequest(request, queryParams: params)
        }
        requestError = executeAndParseJSONRequest(request, successHandler: successHandler)
        return requestError
    }
    
    
    class func addPostDataToRequest(request: NSMutableURLRequest, postData: Dictionary<String, String>) {
        do {
            try request.HTTPBody = NSJSONSerialization.dataWithJSONObject(postData, options: NSJSONWritingOptions.PrettyPrinted)
        } catch {
            let nserror = error as NSError
            NSLog("Unresolved error parsing post data \(nserror), \(nserror.userInfo)")
        }
    }
    
    class func addQueryParamsToRequest(request: NSMutableURLRequest, queryParams: Dictionary<String, String>) {
        if let urlComponents = NSURLComponents(URL: request.URL!, resolvingAgainstBaseURL: true) {
            urlComponents.queryItems = []
            for param in queryParams {
                urlComponents.queryItems?.append(NSURLQueryItem(name: param.0, value:param.1))
            }
            request.URL = urlComponents.URL
        }
    }
    
    class func executeAndParseJSONRequest(request: NSMutableURLRequest, successHandler: (json: NSDictionary)->()) -> NSError? {
        
        var requestError: NSError? = nil
        
        //Create the task
        if requestError == nil {
            let session = NSURLSession.sharedSession()
            let task = session.dataTaskWithRequest(request, completionHandler: {data, response, error -> Void in
                if error != nil {
                    let nserror = error! as NSError
                    requestError = nserror
                    NSLog(" ! ! ! ! ! ! ! Error making JSON Request \(nserror), \(nserror.userInfo), \(response)")
                } else {
                    if (data != nil) {
                        do {
                            let json = try NSJSONSerialization.JSONObjectWithData(data!, options: .MutableLeaves) as? NSDictionary
                            successHandler(json: json!)
                        } catch {
                            let nserror = error as NSError
                            requestError = nserror
                            NSLog(" ! ! ! ! ! ! ! Unresolved error parsing json response \(nserror), \(nserror.userInfo), \(response)")
                        }
                    } else {
                        NSLog(" ! ! ! ! ! ! ! Response returned with no data, \(response)")
                    }
                }
            })
            task.resume()
        }
        return requestError
    }
}
